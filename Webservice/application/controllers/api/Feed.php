<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Feed extends REST_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Content-Type: text/html; charset=ISO-8859-1", true);
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die('FAILURE: OPTIONS DETECTED!');
        }
        // Construct the parent class
        parent::__construct();

        $this->load->model('Feed_Model');
        $this->load->model('Users_Model');
        $this->methods['find_post']['limit'] = 500;
        $this->methods['addPublication_post']['limit'] = 500;
    }

    public function find_post() {

        $index = $this->post('INDEX');
        $id_user = $this->post('ID_USER');
        $lat = $this->post('LATITUDE');
        $long = $this->post('LONGITUDE');
        $descricao = $this->post('DESCRIPTION');
        if ($index === NULL) {
            $this->response("Falta o índice!", REST_Controller::HTTP_NOT_FOUND);
            return;
        }
        if ($lat === NULL) {
            $this->response("Falta a latitude!", REST_Controller::HTTP_NOT_FOUND);
            return;
        }
        if ($long === NULL) {
            $this->response("Falta a longitude!", REST_Controller::HTTP_NOT_FOUND);
            return;
        }
        if ($id_user === NULL) {
            $this->response("Falta o id do usuário", REST_Controller::HTTP_NOT_FOUND);
            return;
        }
        $result['LISTA'] = $this->Feed_Model->find($index, $id_user, $lat, $long, $descricao);

        if ($result['LISTA']) {
            $result['INDEX'] = $index + 10;
            $result['SUCCESS'] = true;
            $result['MESSAGE'] = 'Carregamento realizado com sucesso.';

            $this->response($result, REST_Controller::HTTP_OK);
        } else {
            $resp = new \stdClass();
            $resp->SUCCESS = false;
            if ($descricao !== NULL) {
                $resp->MESSAGE = 'Acredite, procuramos em todos os lugares, mas... Não encontramos nada para sua pesquisa';
            } else {
                $resp->MESSAGE = 'Não há eventos proximo a sua localização.';
            }
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function addPublication_post() {
        $this->load->helper('security');

        try {
            $this->db->trans_begin();

            $ID_PUBLICATION = $this->post('ID_PUBLICATION');

            $date = $this->getDatetimeNow();

            $data = [
                'ID_USER' => $this->post('ID_USER'),
                'DESCRIPTION' => $this->post('DESCRIPTION'),
                'COUNTRY' => $this->post('COUNTRY'),
                'IMAGE' => '',
                'STATE' => $this->post('STATE'),
                'CITY' => $this->post('CITY'),
                'DISTRICT' => $this->post('DISTRICT'),
                'CEP' => $this->post('CEP'),
                'DATE' => $date,
                'LATITUDE' => $this->post('LATITUDE'),
                'LONGITUDE' => $this->post('LONGITUDE')
            ];

            $image = $this->post('IMAGE');            

            if (!$ID_PUBLICATION) {
                $lastId = $this->Feed_Model->insert($data);

                $isBase64 = strpos($image, "data:image");
                if ($isBase64 !== FALSE) {
                    $name = "PUB-" . $lastId;
                    $image = $this->save_base64_image($image, $name);
                    $this->load->helper('url');
                    $this->Feed_Model->updateImageNews($lastId, "http://enjoys.96.lt/rest/common/data/". $image);
                }
                if ($lastId) {
                    $result = true;
                }
            } 
            if ($result) {
                $this->db->trans_commit();

                $resp = new \stdClass();
                $resp->SUCCESS = true;
                $resp->MESSAGE = 'Publicação cadastrada com sucesso.';
                $this->response($resp, REST_Controller::HTTP_OK);
            } else
                throw new Exception("Ocorreu um erro durante o salvamento dos dados.");
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = $e->getMessage();
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
            return;
        }
    }

    public function save_base64_image($base64_image_string, $output_file_without_extension) {
        $splited = explode(',', substr($base64_image_string, 5), 2);
        $mime = $splited[0];
        $data = $splited[1];

        $mime_split_without_base64 = explode(';', $mime, 2);
        $mime_split = explode('/', $mime_split_without_base64[0], 2);
        if (count($mime_split) == 2) {
            $extension = $mime_split[1];
            if ($extension == 'jpeg')
                $extension = 'jpg';
            $output_file_with_extension = $output_file_without_extension . '.jpg';
        }
        file_put_contents(COMMON_PATH . $output_file_with_extension, base64_decode($data));

        return $output_file_with_extension;
    }

    public function like_post() {
        $this->load->helper('security');
                
        try {
            $this->db->trans_begin();

            $data = [
                'ID_USER' => $this->post('ID_USER'),
                'ID_PUBLICATION' => $this->post('ID_PUBLICATION'),
                'DATE' => $this->getDatetimeNow()
            ];           

            if ($this->Feed_Model->check_like($data['ID_USER'], $data['ID_PUBLICATION'])) {
                $this->response(true, REST_Controller::HTTP_NOT_FOUND);
                return;
            }

            $result = $this->Feed_Model->like($data);

            if ($result) {
                $this->db->trans_commit();

                $resp = new \stdClass();
                $resp->SUCCESS = true;
                $resp->MESSAGE = 'sucesso';
                $this->response($resp, REST_Controller::HTTP_OK);
            } else
                throw new Exception("Ocorreu um erro durante o salvamento dos dados.");
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = $e->getMessage();
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
            return;
        }
    }

    public function deslike_post() {
        $this->load->helper('security');

        try {
            $this->db->trans_begin();

            $data = [
                'ID_USER' => $this->post('ID_USER'),
                'ID_PUBLICATION' => $this->post('ID_PUBLICATION'),
                'DATE' => $this->getDatetimeNow()
            ];

            if ($this->Feed_Model->check_deslike($data['ID_USER'], $data['ID_PUBLICATION'])) {
                $this->response(true, REST_Controller::HTTP_NOT_FOUND);
                return;
            }

            $result = $this->Feed_Model->deslike($data);

            if ($result) {
                $this->db->trans_commit();

                $resp = new \stdClass();
                $resp->SUCCESS = true;
                $resp->MESSAGE = 'sucesso';
                $this->response($resp, REST_Controller::HTTP_OK);
            } else
                throw new Exception("Ocorreu um erro durante o salvamento dos dados.");
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = $e->getMessage();
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
            return;
        }
    }

    public function comment_post() {
        $this->load->helper('security');

        try {
            $this->db->trans_begin();

            $data = [
                'ID_USER' => $this->post('ID_USER'),
                'ID_PUBLICATION' => $this->post('ID_PUBLICATION'),
                'DESCRIPTION' => $this->post('DESCRIPTION'),
                'DATE' => $this->getDatetimeNow()
            ];

            $result = $this->Feed_Model->comment($data);

            if ($result) {
                $this->db->trans_commit();

                $resp = new \stdClass();
                $resp->SUCCESS = true;
                $resp->MESSAGE = 'Seu comentário foi salvo com sucesso.';
                $this->response($resp, REST_Controller::HTTP_OK);
            } else
                throw new Exception("Ocorreu um erro durante o salvamento dos dados.");
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = $e->getMessage();
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
            return;
        }
    }

    public function findComments_post() {
        $id_publication = $this->post('ID_PUBLICATION');
        $comments['LISTA'] = $this->Feed_Model->findComments($id_publication);

        if ($comments['LISTA']) {
            $comments['SUCCESS'] = true;
            $comments['MESSAGE'] = 'Carregamento realizado com sucesso.';

            $this->response($comments, REST_Controller::HTTP_OK);
        } else {
            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = 'Não há comentários para esta publicação';
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function getDatetimeNow() {
        $tz_object = new DateTimeZone('Brazil/East');
        //date_default_timezone_set('Brazil/East');

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ H:i:s');
    }

}
