<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller {

    function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die('FAILURE: OPTIONS DETECTED!');
        }
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['login_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function login_post() {
        $this->load->model('Users_Model');
        $login = $this->Users_Model->login($this->post('EMAIL'), $this->post('PASSWORD'));

        if ($login) {
            //inicia a sessão com os dados do usuário
            $this->session->set_userdata("user", $login);
            $login->SUCCESS = true;
            $login->MESSAGE = 'Autenticação realizada com sucesso';
            $this->response($login, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = 'Usuário ou senha incorretos.';
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function load_post() {
        $this->load->model('Users_Model');
        $id = $this->post('ID_USER');
        $user = $this->Users_Model->loadUser($id);

        if ($user) {
            $user->SUCCESS = true;
            $user->MESSAGE = 'Carregamento realizado com sucesso.';
            $this->response($user, REST_Controller::HTTP_OK);
        } else {
            $user->SUCCESS = false;
            $user->MESSAGE = 'Não há dados do usuário informado.';
            $this->response($user, REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function newUser_post() {
        $this->load->helper('security');
        $this->load->library('form_validation');

        $this->form_validation->validation_data = $this->post();
        $this->form_validation->set_error_delimiters('<span class="form_error">', '<br /></span>');
        $this->form_validation->set_rules('NAME', 'NAME', 'required');
        $this->form_validation->set_rules('LAST_NAME', 'LAST_NAME', 'required');
        $this->form_validation->set_rules('EMAIL', 'EMAIL', 'required');
        $this->form_validation->set_rules('PHONE', 'PHONE', 'required');
        $this->form_validation->set_rules('PASSWORD', 'PASSWORD', 'required');
        $this->form_validation->set_rules('AVATAR', 'AVATAR', 'required');

        try {
            $this->db->trans_begin();

            $ID_USER = $this->post('ID_USER');

            $date = $this->getDatetimeNow();

            $data = [
                'NAME' => $this->post('NAME'),
                'LAST_NAME' => $this->post('LAST_NAME'),
                'EMAIL' => $this->post('EMAIL'),
                'PHONE' => $this->post('PHONE'),
                'PASSWORD' => $this->post('PASSWORD'),
                'REGISTER_DATE' => $date,
                'AVATAR' => $this->post('AVATAR'),
            ];

            $this->load->model('Users_Model');

            //TODO: checa se usuario já está cadastrado

            if (!$ID_USER){
                if ($conflicted = $this->Users_Model->check_email($data['EMAIL'])) {
                throw new Exception("Usuário já possui cadastro.");
                }
                $result = $this->Users_Model->insert($data);
            }else
                $result = $this->Users_Model->update($ID_USER, $data);

            if ($result) {
                $this->db->trans_commit();

                $resp = new \stdClass();
                $resp->SUCCESS = true;
                $resp->MESSAGE = 'Usuário cadastrado com sucesso.';
                $this->response($resp, REST_Controller::HTTP_OK);
            } else
                throw new Exception("Ocorreu um erro durante o salvamento dos dados.");
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $resp = new \stdClass();
            $resp->SUCCESS = false;
            $resp->MESSAGE = $e->getMessage();
            $this->response($resp, REST_Controller::HTTP_NOT_FOUND);
            return;
        }
    }

    public function getDatetimeNow() {
        $tz_object = new DateTimeZone('Brazil/East');
        //date_default_timezone_set('Brazil/East');

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ H:i:s');
    }

}
