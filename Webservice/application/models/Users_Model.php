<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	//valida usuario e senha
	public function login($username, $password)
	{
		$this->db->from('USER');
		$this->db->where('EMAIL', $username);
		$this->db->where('PASSWORD', $password);

		return $this->db->get()->first_row();
	}

	public function check_email($whereConflict)
	{
		$this->db->select("1");
		$this->db->from('USER');
		$this->db->where('EMAIL', $whereConflict);

		return $this->db->get()->first_row();
	}

	public function insert($data)
	{
		$this->db->set($data);
		return $this->db->insert('USER');
	}

	public function update($ID_USER, $data)
	{
		$this->db->where('ID_USER', $ID_USER);
		$this->db->set($data);
		$this->db->update('USER');

		return $this->db->affected_rows();
	}

	public function loadUser($id)
	{
		$this->db->from('USER');
		$this->db->where('ID_USER', $id);
		return $this->db->get()->first_row();
	}

}
