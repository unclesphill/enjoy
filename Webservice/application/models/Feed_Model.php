<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feed_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function find($index, $id_user, $lat, $long, $descricao) {
        $this->db->select('
                USER.NAME,
                USER.LAST_NAME,
                USER.AVATAR,
                ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( PUBLICATION.LATITUDE ) ) * cos( radians( PUBLICATION.LONGITUDE ) - radians(' . $long . ') ) + sin( radians(' . $lat . ') ) * sin( radians( PUBLICATION.LATITUDE ) ) ) ) AS DISTANCIA,
                PUBLICATION.ID_PUBLICATION, 
                PUBLICATION.ID_USER, 
                PUBLICATION.DESCRIPTION, 
                PUBLICATION.DATE, 
                PUBLICATION.IMAGE, 
                PUBLICATION.COUNTRY, 
                PUBLICATION.STATE,
                PUBLICATION.CITY,
                PUBLICATION.DISTRICT,
                PUBLICATION.CEP,
                PUBLICATION.LATITUDE,
                PUBLICATION.LONGITUDE,
                (	SELECT 
                                COUNT(*) 
                        FROM PUBLICATION_COMMENTS 
                        WHERE PUBLICATION_COMMENTS.ID_PUBLICATION = PUBLICATION.ID_PUBLICATION
                ) AS QTD_COMMENTS,

                (	SELECT 
                                COUNT(*)
                        FROM PUBLICATION_DESLIKES
                WHERE PUBLICATION_DESLIKES.ID_PUBLICATION = PUBLICATION.ID_PUBLICATION
                ) AS QTD_DESLIKE,

                (	SELECT
                                COUNT(*)
                            FROM PUBLICATION_LIKES
                    WHERE PUBLICATION_LIKES.ID_PUBLICATION = PUBLICATION.ID_PUBLICATION 
                ) AS QTD_LIKES,
                (	SELECT
                                1
                            FROM PUBLICATION_LIKES
                    WHERE PUBLICATION_LIKES.ID_PUBLICATION = PUBLICATION.ID_PUBLICATION
                    AND PUBLICATION_LIKES.ID_USER = ' . $id_user . '
                ) AS ILIKE,
                (	SELECT
                                1
                            FROM PUBLICATION_DESLIKES
                    WHERE PUBLICATION_DESLIKES.ID_PUBLICATION = PUBLICATION.ID_PUBLICATION
                    AND PUBLICATION_DESLIKES.ID_USER = ' . $id_user . '
                ) AS IDESLIKE'
        );
        if ($descricao !== NULL) {
            $this->db->like('DESCRIPTION', $descricao);
        }
        $this->db->join('USER', 'USER.ID_USER = PUBLICATION.ID_USER');
        $this->db->having('DISTANCIA < 25');
        $this->db->order_by('DISTANCIA');
        $this->db->limit(10, $index);

        return $this->db->get('PUBLICATION')->result();
    }

    public function check_email($whereConflict) {
        $this->db->select("1");
        $this->db->from('USER');
        $this->db->where('EMAIL', $whereConflict);

        return $this->db->get()->first_row();
    }

    public function insert($data) {
        $this->db->set($data);
        $this->db->insert('PUBLICATION');
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function updateImageNews($id, $image) {
        $this->db->where('ID_PUBLICATION', $id);
        $this->db->set('IMAGE', $image);
        $this->db->update('PUBLICATION');

        return $this->db->affected_rows();
    }

    public function update($ID_USER, $data) {
        $this->db->where('ID_USER', $ID_USER);
        $this->db->set($data);
        $this->db->update('USER');

        return $this->db->affected_rows();
    }

    public function comment($data) {
        $this->db->set($data);
        return $this->db->insert('PUBLICATION_COMMENTS');
    }

    public function check_like($id_user, $id_publication) {
        $this->db->select("1");
        $this->db->from('PUBLICATION_LIKES');
        $this->db->where('ID_USER', $id_user);
        $this->db->where('ID_PUBLICATION', $id_publication);

        return $this->db->get()->first_row();
    }

    public function check_deslike($id_user, $id_publication) {
        $this->db->select("1");
        $this->db->from('PUBLICATION_DESLIKES');
        $this->db->where('ID_USER', $id_user);
        $this->db->where('ID_PUBLICATION', $id_publication);

        return $this->db->get()->first_row();
    }

    public function like($data) {
        $this->db->set($data);
        return $this->db->insert('PUBLICATION_LIKES');
    }

    public function deslike($data) {
        $this->db->set($data);
        return $this->db->insert('PUBLICATION_DESLIKES');
    }

    public function findComments($id) {
        $this->db->select('PUBLICATION_COMMENTS.ID_COMMENTS,
                           PUBLICATION_COMMENTS.ID_PUBLICATION,
                           USER.ID_USER,
                           PUBLICATION_COMMENTS.DESCRIPTION,
                           PUBLICATION_COMMENTS.DATE,
                           USER.AVATAR,
                           CONCAT(USER.NAME, " ", USER.LAST_NAME) AS NAME');
        $this->db->join('USER', 'USER.ID_USER = PUBLICATION_COMMENTS.ID_USER');
        $this->db->where('ID_PUBLICATION', $id);
        $this->db->order_by("PUBLICATION_COMMENTS.DATE","ASC");
        return $this->db->get('PUBLICATION_COMMENTS')->result();
    }
}
