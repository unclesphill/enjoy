package br.com.unclephill.enjoy.object;

public class ObLocation {
    private long ID_LOCATION;
    private double LATITUDE;
    private double LOGITUDE;
    private String CEP;
    private String COUNTRY;
    private String STATE;
    private String CITY;
    private String DISTRICT;
    private String MESSAGE;
    private Boolean SUCESS;

    public ObLocation(){
        this.ID_LOCATION=0;
        this.LATITUDE=0;
        this.LOGITUDE=0;
        this.CEP = "";
        this.COUNTRY = "";
        this.STATE = "";
        this.CITY = "";
        this.DISTRICT = "";
        this.MESSAGE = "";
        this.SUCESS=false;
    }

    public ObLocation(long ID_LOCATION,double LATITUDE,double LOGITUDE,
                      String CEP,String COUNTRY,String STATE, String CITY,
                      String DISTRICT,String MESSAGE,boolean SUCESS){
        this.ID_LOCATION = ID_LOCATION;
        this.LATITUDE = LATITUDE;
        this.LOGITUDE= LOGITUDE;
        this.CEP = CEP;
        this.STATE = STATE;
        this.CITY = CITY;
        this.DISTRICT = DISTRICT;
        this.COUNTRY = COUNTRY;
        this.MESSAGE = MESSAGE;
        this.SUCESS = SUCESS;
    }

    public double getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(double LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public double getLOGITUDE() {
        return LOGITUDE;
    }

    public void setLOGITUDE(double LOGITUDE) {
        this.LOGITUDE = LOGITUDE;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getDISTRICT() {
        return DISTRICT;
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public Boolean isSUCESS() {
        return SUCESS;
    }

    public void setSUCESS(Boolean SUCESS) {
        this.SUCESS = SUCESS;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public long getID_LOCATION() {
        return ID_LOCATION;
    }

    public void setID_LOCATION(long ID_LOCATION) {
        this.ID_LOCATION = ID_LOCATION;
    }

}
