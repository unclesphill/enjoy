package br.com.unclephill.enjoy.view.activity;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.business.BsMaps;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.repository.webservice.WsGetPublication;
import br.com.unclephill.enjoy.view.fragment.VwFgFeed;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.unclephill.enjoy.app.ApFunctions.base64ParseImage;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarFragment;
import static br.com.unclephill.enjoy.app.ApFunctions.logOut;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.LL_ContainerFragment;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.SEARCH_FEED;
import static br.com.unclephill.enjoy.app.ApSession.LL_UpdateFeed;
import static br.com.unclephill.enjoy.app.ApSession.LL_Feed;
import static br.com.unclephill.enjoy.app.ApSession.SC_SwipeContainer;
import static br.com.unclephill.enjoy.app.ApSession.theFirstAcess;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;

import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class VwAcFeed extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        LoaderManager.LoaderCallbacks<Cursor> ,SwipeRefreshLayout.OnRefreshListener {
    private Toolbar idTbFeed;
    private Toolbar idTbSearch;
    private DrawerLayout idDwFeed;
    private NavigationView idNwFeed;
    private CircleImageView idImgProfilePhoto ;
    private TextView idTxwName;
    private TextView idTxwEmail;
    private View idVwNvDwHeader;
    private ImageButton idBtnSearch;
    private ImageButton idNewSearch;
    private EditText idEdtSearch;
    private LinearLayout idLlUpdateFeed;
    private LinearLayout idLlFeed;
    private RelativeLayout idContainerFragment;
    private SwipeRefreshLayout idSwipeContainer;
    BsMaps bsMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.inflar();
        this.startInformationUser();
    }

    private void inflar(){
        this.bsMaps = new BsMaps(VwAcFeed.this);
        if (LOCATION_SESSION.getID_LOCATION() <= 0){
                this.bsMaps.showAlertGPS();
        }

        setContentView(R.layout.vw_dw_feed);
        iniciarFragment(new VwFgFeed(),R.id.idContainerFragment,getSupportFragmentManager());

        this.idLlUpdateFeed = (LinearLayout) findViewById(R.id.idLlUpdateFeed);
        this.idLlFeed = (LinearLayout) findViewById(R.id.idLlFeed);
        this.idContainerFragment = (RelativeLayout) findViewById(R.id.idContainerFragment);
        this.idSwipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        this.idSwipeContainer.setOnRefreshListener(this);
        LL_UpdateFeed = this.idLlUpdateFeed;
        LL_Feed = this.idLlFeed;
        LL_ContainerFragment = this.idContainerFragment;
        SC_SwipeContainer = this.idSwipeContainer;

        this.idTbFeed = (Toolbar) findViewById(R.id.idTbFeed);
        this.idTbSearch = (Toolbar) findViewById(R.id.idTbSearch);
        setSupportActionBar(this.idTbFeed);
        setSupportActionBar(this.idTbSearch);

        this.idDwFeed = (DrawerLayout) findViewById(R.id.idDwFeed);
        ActionBarDrawerToggle acdToggle = new ActionBarDrawerToggle(VwAcFeed.this,this.idDwFeed,this.idTbFeed,0,0);
        acdToggle.syncState();

        this.idNwFeed = (NavigationView) findViewById(R.id.idNwFeed);
        this.idNwFeed.setNavigationItemSelectedListener(VwAcFeed.this);
        this.idNwFeed.inflateMenu(R.menu.mn_dw_feed);

        this.idVwNvDwHeader = this.idNwFeed.getHeaderView(0);
        this.idImgProfilePhoto = (CircleImageView) idVwNvDwHeader.findViewById(R.id.idImgProfilePhoto);
        this.idTxwName = (TextView) idVwNvDwHeader.findViewById(R.id.idTxwName);
        this.idTxwEmail = (TextView) idVwNvDwHeader.findViewById(R.id.idTxwEmail);
        this.idBtnSearch = (ImageButton) findViewById(R.id.idBtnSearch);
        this.idEdtSearch = (EditText) findViewById(R.id.idEdtSearch);
    }

    private void startInformationUser(){
        Bitmap bmp = base64ParseImage(USER_SESSION.getAVATAR().replace("data:image/jpg;base64,",""));
        if (bmp!=null){
            this.idImgProfilePhoto.setImageBitmap(bmp);
            this.idImgProfilePhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }else{
            if(!USER_SESSION.getAVATAR().equals("")){
                Picasso.with(this).load(USER_SESSION.getAVATAR()).into(this.idImgProfilePhoto);
                this.idImgProfilePhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
        this.idTxwEmail.setText(USER_SESSION.getEMAIL());
        this.idTxwName.setText(USER_SESSION.getNAME() + " " + USER_SESSION.getLAST_NAME());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.idItemGps:
                iniciarActivity(VwAcFeed.this,VwAcLocation.class,null);
                break;
            case R.id.idItemSettings:
                iniciarActivity(VwAcFeed.this,VwAcUser.class,null);
                break;
            case R.id.idItemLogout:
                logOut(VwAcFeed.this);
                fecharActivity(VwAcFeed.this);
        }
        return false;
    }

    @Override
    public void onResume(){
        super.onResume();
        this.startInformationUser();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.idDwFeed);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            SEARCH_FEED = "";
            LIST_PUBLICATION_SESSION = new ObListPublication();
            LIST_PUBLICATION = new ObListPublication();
        }
    }

    public void onClickNewPublication(View view){
        iniciarActivity(VwAcFeed.this,VwAcNewPublication.class,null);
    }

    public void onClickNewSearch(View view){
        this.idTbFeed.setVisibility(View.INVISIBLE);
        this.idTbSearch.setVisibility(View.VISIBLE);
    }

    public void onClickPushPublication(View view){
        theFirstAcess = true;
        new WsGetPublication(VwAcFeed.this).execute();
    }

    public void onClickSearch(View view){
        try{
            if (this.idEdtSearch.getText().toString().equals("")){
                modalNeutro(VwAcFeed.this,"Atenção!","Informe uma pesquisa!","OK");
                this.idEdtSearch.requestFocus();
                return;
            }

            theFirstAcess = true;
            SEARCH_FEED = this.idEdtSearch.getText().toString();
            new WsGetPublication(VwAcFeed.this).execute();
        }catch (Exception ex){
            excecoes(VwAcFeed.this,ex);
        }
    }

    public void onClickCallBack(View view){
        SEARCH_FEED = "";
        this.idTbFeed.setVisibility(View.VISIBLE);
        this.idTbSearch.setVisibility(View.INVISIBLE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        new WsGetPublication(VwAcFeed.this).execute();
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onRefresh() {
        LIST_PUBLICATION.setINDEX(0);
        new WsGetPublication(VwAcFeed.this).execute();
    }
}
