package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.object.ObUser;
import br.com.unclephill.enjoy.repository.webservice.WsLogin;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.ACESS_TOKEN;
import static br.com.unclephill.enjoy.app.ApSession.TB_USER;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class VwAcLogin extends Activity {
    private EditText idEdtEmail;
    private EditText idEdtPassword;
    private Button idBtnLogin;
    private Button idBtnCadastrar;
    private CallbackManager callbackManager;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.inflar();
    }

    private void inflar(){
        this.setContentView(R.layout.vw_ac_login);
        this.callbackManager = CallbackManager.Factory.create();

        this.idEdtEmail = (EditText) this.findViewById(R.id.idEdtEmail);
        this.idEdtPassword = (EditText) this.findViewById(R.id.idEdtPassword);
        this.idBtnLogin = (Button) this.findViewById(R.id.idBtnLogin);
        this.idBtnCadastrar = (Button) this.findViewById(R.id.idBtnCadastrar);
        this.loginButton = (LoginButton) this.findViewById(R.id.login_button);
        this.loginButton.setReadPermissions("public_profile","email");

        this.loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ACESS_TOKEN = AccessToken.getCurrentAccessToken();
                requestProfile(loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                Toast.makeText(VwAcLogin.this,"Login cancelado!",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(VwAcLogin.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void requestProfile(final String idUser){
        GraphRequest request = new GraphRequest(ACESS_TOKEN.getCurrentAccessToken(), idUser, null, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                try{
                    Profile profile = Profile.getCurrentProfile();
                    USER_SESSION.setID_USER(Long.parseLong(profile.getId()));
                    USER_SESSION.setNAME(profile.getFirstName());
                    USER_SESSION.setLAST_NAME(profile.getMiddleName() + " " + profile.getLastName());
                    USER_SESSION.setEMAIL(response.getJSONObject().getString("email"));
                    USER_SESSION.setAVATAR(profile.getProfilePictureUri(80,80).toString());
                    USER_SESSION.setPASSWORD("******");
                    USER_SESSION.setSUCESS(true);
                    USER_SESSION.setMESSAGE("Sucesso!");

                    if (TB_USER.select().size() <= 0){
                        TB_USER.deleteAll();
                        TB_USER.insert(USER_SESSION);
                    }

                    iniciarActivity(VwAcLogin.this ,VwAcFeed.class,null);
                    fecharActivity(VwAcLogin.this);
                }catch (Exception ex){
                    Toast.makeText(VwAcLogin.this,"Falha ao carregar os dados do usuário!",Toast.LENGTH_LONG).show();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void onClickLogin(View view){
        try{
            if (!this.validaView()){return;}
            USER_SESSION = new ObUser();
            USER_SESSION.setEMAIL(idEdtEmail.getText().toString());
            USER_SESSION.setPASSWORD(idEdtPassword.getText().toString());
            new WsLogin(VwAcLogin.this).execute();
        }catch (Exception ex){
            excecoes(this,ex);
        }
    }

    public void onClickCadastrar(View view){
        iniciarActivity(VwAcLogin.this,VwAcUser.class,null);
    }

    private boolean validaView(){
        if (this.idEdtEmail.getText().toString().trim().equals("")){
            modalNeutro(VwAcLogin.this,"Atenção!","Informe o e-mail.","OK");
            this.idEdtEmail.requestFocus();
            return false;
        }

        if (this.idEdtPassword.getText().toString().trim().equals("")){
            modalNeutro(VwAcLogin.this,"Atenção!","Informe a senha.","OK");
            this.idEdtPassword.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
