package br.com.unclephill.enjoy.business;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

import br.com.unclephill.enjoy.repository.database.TbLocation;
import br.com.unclephill.enjoy.view.activity.VwAcLocation;

import static android.support.v4.content.ContextCompat.startActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.TB_LOCATION;

public class BsMaps implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient gApiClient;
    private Activity context;

    public BsMaps(Activity context){
        this.context = context;
    }

    public boolean checkGPS(){
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void showAlertGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Para usar a aplicação é fundamental obter a localização.\nDeseja prosseguir?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        iniciarActivity(context, VwAcLocation.class,null);
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void showAlertActivateGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("GPS desativado. Deseja ativar?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        showMenuGPS();
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(gApiClient);
            if (location != null) {
                LOCATION_SESSION.setLATITUDE(location.getLatitude());
                LOCATION_SESSION.setLOGITUDE(location.getLongitude());
                getAddress(location.getLatitude(), location.getLongitude());
            } else {
                modalNeutro(context, "Atenção!", "Não foi possível obter a localização.\nInforme manualmente ou tente novamente.", "OK");
            }
        }catch (Exception ex){
            excecoes(context,ex);
        }
    }

    public synchronized void getLocation(){
        this.gApiClient = new GoogleApiClient.Builder(context)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        this.gApiClient.connect();
    }

    private void getAddress(double latitude, double longitude) throws Exception{
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());;
            List<Address> address = geocoder.getFromLocation(latitude,longitude,1);
            if (address != null) {
                LOCATION_SESSION.setCOUNTRY(address.get(0).getCountryName());
                LOCATION_SESSION.setSTATE(address.get(0).getAdminArea());
                LOCATION_SESSION.setCITY(address.get(0).getLocality());
                LOCATION_SESSION.setCEP(address.get(0).getPostalCode());
                LOCATION_SESSION.setDISTRICT(address.get(0).getSubLocality());
                LOCATION_SESSION.setSUCESS(true);
                LOCATION_SESSION.setMESSAGE("Sucesso!");
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void getAddress()throws Exception{
        try {
            String location = LOCATION_SESSION.getDISTRICT() +
                    " " + LOCATION_SESSION.getCITY() +
                    " " + LOCATION_SESSION.getSTATE() +
                    " " + LOCATION_SESSION.getCOUNTRY() +
                    " " + LOCATION_SESSION.getCEP();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());;
            List<Address> address = geocoder.getFromLocationName(location,1);
            if (address != null) {
                LOCATION_SESSION.setLATITUDE(address.get(0).getLatitude());
                LOCATION_SESSION.setLOGITUDE(address.get(0).getLongitude());
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void showMenuGPS(){
        startActivity(context,new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),null);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}


