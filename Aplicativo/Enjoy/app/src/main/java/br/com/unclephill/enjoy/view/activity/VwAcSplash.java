package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

import java.util.List;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.business.BsMaps;
import br.com.unclephill.enjoy.object.ObLocation;
import br.com.unclephill.enjoy.object.ObUser;
import br.com.unclephill.enjoy.repository.database.TbLocation;
import br.com.unclephill.enjoy.repository.database.TbPublication;
import br.com.unclephill.enjoy.repository.database.TbStart;
import br.com.unclephill.enjoy.repository.database.TbUser;

import static br.com.unclephill.enjoy.app.ApSession.ACESS_TOKEN;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.TB_LOCATION;
import static br.com.unclephill.enjoy.app.ApSession.TB_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.TB_START;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.TB_USER;
import static br.com.unclephill.enjoy.app.ApSession.START_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.MAPS;

public class VwAcSplash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vw_ac_splash);

        this.loadUser();
        this.loadLocation();
        this.loadStart();
        this.loadPublication();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent it;

                ACESS_TOKEN = AccessToken.getCurrentAccessToken();
                Boolean LoginFacebookExpirado;
                if (ACESS_TOKEN == null){LoginFacebookExpirado = true;
                }else{LoginFacebookExpirado = ACESS_TOKEN.isExpired();}

                if (USER_SESSION.getID_USER()<=0  && LoginFacebookExpirado){
                    if (!START_SESSION){
                        it = new Intent(VwAcSplash.this,
                                VwAcWelcome.class);
                    }else{
                        it = new Intent(VwAcSplash.this,
                                VwAcLogin.class);
                    }
                }else{
                    it = new Intent(VwAcSplash.this,VwAcFeed.class);
                }
                VwAcSplash.this.startActivity(it);
                VwAcSplash.this.finish();
            }
        }, 3000);
    }

    private void loadUser(){
        TB_USER = new TbUser(VwAcSplash.this);
        List<ObUser> lObUser = TB_USER.select();
        if (lObUser != null) {
            if (lObUser.size() > 0) {
                if (lObUser.get(0).getID_USER() > 0) {
                    USER_SESSION.setID_USER(lObUser.get(0).getID_USER());
                    USER_SESSION.setNAME(lObUser.get(0).getNAME());
                    USER_SESSION.setLAST_NAME(lObUser.get(0).getLAST_NAME());
                    USER_SESSION.setEMAIL(lObUser.get(0).getEMAIL());
                    USER_SESSION.setPASSWORD(lObUser.get(0).getPASSWORD());
                    USER_SESSION.setPHONE(lObUser.get(0).getPHONE());
                    USER_SESSION.setAVATAR(lObUser.get(0).getAVATAR());
                    USER_SESSION.setMESSAGE("Sucesso!");
                    USER_SESSION.setSUCESS(true);
                }
            }
        }
    }

    private void loadLocation(){
        TB_LOCATION = new TbLocation(VwAcSplash.this);
        List<ObLocation> lObLocation = TB_LOCATION.select();
        if (lObLocation != null) {
            if (lObLocation.size() > 0) {
                if (lObLocation.get(0).getID_LOCATION() > 0) {
                    LOCATION_SESSION.setID_LOCATION(lObLocation.get(0).getID_LOCATION());
                    LOCATION_SESSION.setDISTRICT(lObLocation.get(0).getDISTRICT());
                    LOCATION_SESSION.setSTATE(lObLocation.get(0).getSTATE());
                    LOCATION_SESSION.setCOUNTRY(lObLocation.get(0).getCOUNTRY());
                    LOCATION_SESSION.setCEP(lObLocation.get(0).getCEP());
                    LOCATION_SESSION.setCITY(lObLocation.get(0).getCITY());
                    LOCATION_SESSION.setLATITUDE(lObLocation.get(0).getLATITUDE());
                    LOCATION_SESSION.setLOGITUDE(lObLocation.get(0).getLOGITUDE());
                    LOCATION_SESSION.setSUCESS(true);
                    LOCATION_SESSION.setMESSAGE("Sucesso!");
                }
            }
        }
    }

    private void loadStart(){
        TB_START = new TbStart(VwAcSplash.this);
        START_SESSION = TB_START.select();
    }

    private void loadPublication(){
        TB_PUBLICATION = new TbPublication(VwAcSplash.this);
    }
}
