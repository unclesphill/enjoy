package br.com.unclephill.enjoy.object;

import java.util.ArrayList;

public class ObListComments {
    private ArrayList<ObComments> LISTA;
    private boolean SUCCESS;
    private String MESSAGE;

    public ObListComments(){
        this.LISTA = new ArrayList<ObComments>();
        this.SUCCESS = false;
        this.MESSAGE = "";
    }

    public ObListComments(ArrayList<ObComments> LISTA,boolean SUCCESS,String MESSAGE){
        this.LISTA = LISTA;
        this.SUCCESS = SUCCESS;
        this.MESSAGE = MESSAGE;
    }

    public ArrayList<ObComments> getLISTA() {
        return LISTA;
    }

    public ObComments getLISTA(int index) {
        return LISTA.get(index);
    }

    public void setLISTA(ArrayList<ObComments> LISTA) {
        this.LISTA = LISTA;
    }

    public void setLISTA(ObComments COMMENTS) {
        this.LISTA.add(COMMENTS);
    }

    public boolean isSUCCESS() {
        return SUCCESS;
    }

    public void setSUCCESS(boolean SUCCESS) {
        this.SUCCESS = SUCCESS;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }
}
