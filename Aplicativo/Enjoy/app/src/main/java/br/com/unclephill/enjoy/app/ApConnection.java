package br.com.unclephill.enjoy.app;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ApConnection {

    public enum TypeConnection{
        POST,GET;
    }

    public static JSONObject Connection(String url, JSONObject obj,TypeConnection typeConnection ) throws Exception {
        try {
            ApConnection apConnection = new ApConnection();
            if (typeConnection == TypeConnection.POST){
                return apConnection.POST(url, obj);
            }else if (typeConnection == TypeConnection.GET){
                return apConnection.GET(url);
            }else{
                return null;
            }
        }catch (Exception ex){
            throw ex;
        }
    }

    private static JSONObject GET(String url)throws Exception {
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            InputStream in = httpclient.execute(request).getEntity().getContent();
            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(in));
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return new JSONObject(sb.toString());
        }catch (Exception ex){
            throw new Exception("Houve um erro na conexão com internet. Tente novamente!");
        }
    }

    private JSONObject POST(String url, JSONObject obj) throws Exception {
        try {
            HttpContext context = new BasicHttpContext();
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 50000);
            HttpClient client = new DefaultHttpClient(params);
            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept-Charset","utf-8");
            if ((!obj.toString().equals("")) && (obj != null) ){post.setEntity(new StringEntity(obj.toString(),"UTF-8"));}
            HttpResponse response = client.execute(post, context);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            String resultString = convertStreamToString(stream);
            stream.close();
            return new JSONObject(resultString);
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    private String convertStreamToString(InputStream stream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sBuilder = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
                e.printStackTrace();
            }
        }
        return sBuilder.toString();
    }
}
