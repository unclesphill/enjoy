package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import br.com.unclephill.enjoy.R;

import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.TB_START;

public class VwAcWelcome extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vw_ac_welcome);
    }

    public void onClickIniciar(View view){
        try{
            TB_START.insert(true);
            iniciarActivity(VwAcWelcome.this,VwAcLogin.class,null);
            fecharActivity(VwAcWelcome.this);
        }catch (Exception ex){
            modalNeutro(VwAcWelcome.this,"Atenção!","Erro: " + ex.getMessage(),"OK");
        }
    }
}
