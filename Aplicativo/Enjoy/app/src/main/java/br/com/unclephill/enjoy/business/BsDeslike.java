package br.com.unclephill.enjoy.business;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.unclephill.enjoy.object.ObDeslike;

public class BsDeslike {
    private ObDeslike obDeslike;

    public BsDeslike(ObDeslike obLike){
        this.obDeslike = obLike;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();
        if (this.obDeslike.getID_USER() > 0){
            json.put("ID_USER",this.obDeslike.getID_USER());
        }
        if(this.obDeslike.getID_PUBLICATION() > 0){
            json.put("ID_PUBLICATION",this.obDeslike.getID_PUBLICATION());
        }
        return json;
    }

    public void setJSON(JSONObject json) throws JSONException {
        try{
            if (json.toString().contains("ID_USER")){this.obDeslike.setID_USER(json.getInt("ID_USER"));}
            if (json.toString().contains("ID_PUBLICATION")){this.obDeslike.setID_PUBLICATION(json.getInt("ID_PUBLICATION"));}
            if (json.toString().contains("SUCCESS")){this.obDeslike.setSUCESS(json.getBoolean("SUCCESS"));}
            if (json.toString().contains("MESSAGE")){this.obDeslike.setMESSAGE(json.getString("MESSAGE"));}
        }catch (Exception ex){
            throw  ex;
        }
    }
}
