package br.com.unclephill.enjoy.business;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import br.com.unclephill.enjoy.object.ObPublication;

public class BsPublication {
    private ObPublication obPublication;

    public  BsPublication(ObPublication obPublication){
        this.obPublication = obPublication;
    }

    public JSONObject getJSON() throws JSONException {
        try{
            JSONObject json = new JSONObject();

            if (this.obPublication.getID_PUBLICATION() > 0){
                json.put("ID_PUBLICATION",this.obPublication.getID_PUBLICATION());
            }
            if (this.obPublication.getID_USER() > 0){
                json.put("ID_USER",this.obPublication.getID_USER());
            }
            json.put("DESCRIPTION",this.obPublication.getDESCRIPTION());
            if (this.obPublication.getIMAGE() != null){
                json.put("IMAGE",this.obPublication.getIMAGE());
            }else{
                json.put("IMAGE","");
            }
            json.put("COUNTRY",this.obPublication.getCOUNTRY());
            json.put("STATE",this.obPublication.getSTATE());
            json.put("CITY",this.obPublication.getCITY());
            json.put("DISTRICT",this.obPublication.getDISTRICT());
            json.put("CEP",this.obPublication.getCEP());
            json.put("LATITUDE",this.obPublication.getLATITUDE());
            json.put("LONGITUDE",this.obPublication.getLONGITUDE());

            return   json;
        }catch (Exception ex){
            throw  ex;
        }
    }

    public void setJSON(JSONObject json) throws JSONException {
        try{
            if (json.toString().contains("ID_PUBLICATION")){this.obPublication.setID_PUBLICATION(json.getInt("ID_PUBLICATION"));}
            if (json.toString().contains("ID_USER")){this.obPublication.setID_USER(json.getInt("ID_USER"));}
            if (json.toString().contains("DESCRIPTION")){this.obPublication.setDESCRIPTION(json.getString("DESCRIPTION"));}
            if (json.toString().contains("IMAGE")){this.obPublication.setIMAGE(json.getString("IMAGE"));}
            if (json.toString().contains("COUNTRY")){this.obPublication.setCOUNTRY(json.getString("COUNTRY"));}
            if (json.toString().contains("CITY")){this.obPublication.setCITY(json.getString("CITY"));}
            if (json.toString().contains("DISTRICT")){this.obPublication.setDISTRICT(json.getString("DISTRICT"));}
            if (json.toString().contains("STATE")){this.obPublication.setSTATE(json.getString("STATE"));}
            if (json.toString().contains("CEP")){this.obPublication.setCEP(json.getString("CEP"));}
            if (json.toString().contains("LATITUDE")){this.obPublication.setLATITUDE(json.getDouble("LATITUDE"));}
            if (json.toString().contains("LONGITUDE")){this.obPublication.setLONGITUDE(json.getDouble("LONGITUDE"));}
            if (json.toString().contains("SUCCESS")){this.obPublication.setSUCESS(json.getBoolean("SUCCESS"));}
            if (json.toString().contains("MESSAGE")){this.obPublication.setMESSAGE(json.getString("MESSAGE"));}
        }catch (Exception ex){
            throw  ex;
        }
    }
}
