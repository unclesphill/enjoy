package br.com.unclephill.enjoy.business;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.unclephill.enjoy.object.ObComments;
import br.com.unclephill.enjoy.object.ObLike;

public class BsComments {
    private ObComments obComments;

    public BsComments(ObComments obComments){
        this.obComments = obComments;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();
        if (this.obComments.getID_USER() > 0){
            json.put("ID_USER",this.obComments.getID_USER());
        }
        if(this.obComments.getID_PUBLICATION() > 0){
            json.put("ID_PUBLICATION",this.obComments.getID_PUBLICATION());
        }
        json.put("DESCRIPTION",this.obComments.getDESCRIPTION());
        return json;
    }

    public void setJSON(JSONObject json) throws JSONException {
        try{
            if (json.toString().contains("ID_USER")){this.obComments.setID_USER(json.getInt("ID_USER"));}
            if (json.toString().contains("ID_PUBLICATION")){this.obComments.setID_PUBLICATION(json.getInt("ID_PUBLICATION"));}
            if (json.toString().contains("DESCRIPTION")){this.obComments.setDESCRIPTION(json.getString("DESCRIPTION"));}
            if (json.toString().contains("SUCCESS")){this.obComments.setSUCESS(json.getBoolean("SUCCESS"));}
            if (json.toString().contains("MESSAGE")){this.obComments.setMESSAGE(json.getString("MESSAGE"));}
        }catch (Exception ex){
            throw  ex;
        }
    }
}
