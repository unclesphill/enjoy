package br.com.unclephill.enjoy.business;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import br.com.unclephill.enjoy.object.ObUser;
import br.com.unclephill.enjoy.repository.database.TbUser;

import static br.com.unclephill.enjoy.app.ApSession.TB_USER;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class BsUser {
    private ObUser obUser;

    public BsUser(ObUser obUser){
        this.obUser = obUser;
    }

    public JSONObject getJSON() throws JSONException {
        try{
            JSONObject json = new JSONObject();

            if (this.obUser.getID_USER() > 0){json.put("ID_USER",this.obUser.getID_USER());}
            if (!this.obUser.getNAME().equals("")){json.put("NAME",this.obUser.getNAME());}
            if (!this.obUser.getLAST_NAME().equals("")){json.put("LAST_NAME",this.obUser.getLAST_NAME());}
            if (!this.obUser.getEMAIL().equals("")){json.put("EMAIL",this.obUser.getEMAIL());}
            if (!this.obUser.getPHONE().equals("")){json.put("PHONE",this.obUser.getPHONE());}
            if (!this.obUser.getPASSWORD().equals("")){json.put("PASSWORD",this.obUser.getPASSWORD());}
            if (!this.obUser.getAVATAR().equals("")){json.put("AVATAR",this.obUser.getAVATAR());}

            return json;
        }catch (Exception ex){
            throw  ex;
        }
    }

    public void setJSON(JSONObject json) throws JSONException {
        try{
            if (json.toString().contains("ID_USER")){this.obUser.setID_USER(json.getInt("ID_USER"));}
            if (json.toString().contains("NAME")){this.obUser.setNAME(json.getString("NAME"));}
            if (json.toString().contains("LAST_NAME")){this.obUser.setLAST_NAME(json.getString("LAST_NAME"));}
            if (json.toString().contains("EMAIL")){this.obUser.setEMAIL(json.getString("EMAIL"));}
            if (json.toString().contains("PHONE")){this.obUser.setPHONE(json.getString("PHONE"));}
            if (json.toString().contains("PASSWORD")){this.obUser.setPASSWORD(json.getString("PASSWORD"));}
            if (json.toString().contains("AVATAR")){this.obUser.setAVATAR(json.getString("AVATAR"));}
            if (json.toString().contains("SUCCESS")){this.obUser.setSUCESS(json.getBoolean("SUCCESS"));}
            if (json.toString().contains("MESSAGE")){this.obUser.setMESSAGE(json.getString("MESSAGE"));}

            TB_USER.deleteAll();
            TB_USER.insert(this.obUser);
        }catch (Exception ex){
            throw  ex;
        }
    }
}
