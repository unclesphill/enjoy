package br.com.unclephill.enjoy.repository.webservice;

import android.content.Context;
import android.os.AsyncTask;
import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsListPublication;
import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.loadPublication;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.TB_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;
import static br.com.unclephill.enjoy.app.ApSession.theFirstAcess;

public class WsGetPublication extends AsyncTask<Void,Void,Void> {
    private BsListPublication bsListPublication;
    private Context context;
    private String URL = URL_BASE + "feed/find/";
    private Boolean Start;

    public WsGetPublication(Context context){
        this.Start = false;
        this.context = context;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        if (theFirstAcess){
            mostrarProgresso(context);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (!Start){
            Start = true;
            try{
                if (LIST_PUBLICATION.getINDEX() == -1 && !theFirstAcess) {return null;}
                this.bsListPublication = new BsListPublication(LIST_PUBLICATION,context);
                this.bsListPublication.setJSON(
                        Connection(
                                URL,this.bsListPublication.getJSON(),
                                ApConnection.TypeConnection.POST)
                );
            }catch (Exception ex){
                try{
                    LIST_PUBLICATION = TB_PUBLICATION.select("");
                    if (LIST_PUBLICATION.getLISTA().size() <= 0){
                        LIST_PUBLICATION.setINDEX(0);
                        LIST_PUBLICATION.setSUCCESS(false);
                        LIST_PUBLICATION.setMESSAGE(ex.getMessage());
                    }
                }catch (Exception e){
                    LIST_PUBLICATION.setINDEX(0);
                    LIST_PUBLICATION.setSUCCESS(false);
                    LIST_PUBLICATION.setMESSAGE(ex.getMessage());
                }
            }
            Start = false;
        }else{
            LIST_PUBLICATION.setSUCCESS(false);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
        try{
            loadPublication(context);
            //load(context);
        }catch (Exception ex){
            excecoes(context,ex);
        }finally {
            if (theFirstAcess){
                theFirstAcess = false;
                fecharProgresso();
            }
        }
    }
}
