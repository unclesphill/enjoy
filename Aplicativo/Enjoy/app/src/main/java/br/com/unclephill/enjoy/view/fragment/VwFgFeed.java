package br.com.unclephill.enjoy.view.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApFunctions;
import br.com.unclephill.enjoy.repository.webservice.WsGetPublication;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.RV_Feed;
import static br.com.unclephill.enjoy.app.ApSession.theFirstAcess;

public class VwFgFeed extends Fragment implements ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack{
    private RecyclerView rvFeed;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.vw_fg_feed, container, false);
        this.inflar(view);
        return view;
    }

    private void inflar(View view){
        this.rvFeed = (RecyclerView) view.findViewById(R.id.idRvFeed);
        this.rvFeed.setHasFixedSize(true);
        this.rvFeed.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llm = (LinearLayoutManager) rvFeed.getLayoutManager();
                RV_Feed = rvFeed;
                if (LIST_PUBLICATION_SESSION.getLISTA().size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    new WsGetPublication(getActivity()).execute();
                }
            }
        });

        this.rvFeed.addOnItemTouchListener(new ApFunctions.RecyclerViewTouchListener( getActivity(), this.rvFeed, this ));
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.rvFeed.setLayoutManager(llm);
        RV_Feed = this.rvFeed;
        if (LOCATION_SESSION.getID_LOCATION() > 0) {
            theFirstAcess = true;
            new WsGetPublication(getActivity()).execute();
        }
    }

    @Override
    public void onClickListener(View view, int position) {

    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }
}
