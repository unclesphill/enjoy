package br.com.unclephill.enjoy.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.unclephill.enjoy.app.ApDatabase;
import br.com.unclephill.enjoy.object.ObLocation;

public class TbLocation {

    private SQLiteDatabase DB_ENJOY;
    private String TB_LOCATION = "LOCATION";

    private enum FIELDS{
        ID_LOCATION,
        LATITUDE,
        LOGITUDE,
        CEP,
        COUNTRY,
        STATE,
        CITY,
        DISTRICT
    }

    public TbLocation(Context context){
        ApDatabase database = new ApDatabase(context);
        this.DB_ENJOY = database.getWritableDatabase();
    }

    public long insert(ObLocation obLocation){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(FIELDS.LATITUDE.name(),obLocation.getLATITUDE());
            contentValues.put(FIELDS.LOGITUDE.name(),obLocation.getLOGITUDE());
            contentValues.put(FIELDS.CEP.name(),obLocation.getCEP());
            contentValues.put(FIELDS.COUNTRY.name(),obLocation.getCOUNTRY());
            contentValues.put(FIELDS.STATE.name(),obLocation.getSTATE());
            contentValues.put(FIELDS.CITY.name(),obLocation.getCITY());
            contentValues.put(FIELDS.DISTRICT.name(),obLocation.getDISTRICT());

            return this.DB_ENJOY.insert(this.TB_LOCATION,null,contentValues);
        }catch (Exception e){
            return 0;
        }
    }

    public Boolean update(ObLocation obLocation){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(FIELDS.LATITUDE.name(),obLocation.getLATITUDE());
            contentValues.put(FIELDS.LOGITUDE.name(),obLocation.getLOGITUDE());
            contentValues.put(FIELDS.CEP.name(),obLocation.getCEP());
            contentValues.put(FIELDS.COUNTRY.name(),obLocation.getCOUNTRY());
            contentValues.put(FIELDS.STATE.name(),obLocation.getSTATE());
            contentValues.put(FIELDS.CITY.name(),obLocation.getCITY());
            contentValues.put(FIELDS.DISTRICT.name(),obLocation.getDISTRICT());

            this.DB_ENJOY.update(this.TB_LOCATION,contentValues,FIELDS.ID_LOCATION.name() + " = ? ",
                    new String[]{Long.toString(obLocation.getID_LOCATION())});
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public Boolean delete(ObLocation obLocation){
        try{
            this.DB_ENJOY.delete(this.TB_LOCATION,FIELDS.ID_LOCATION.name() + " = ? ",
                    new String[]{Long.toString(obLocation.getID_LOCATION())});
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public List<ObLocation> select(){
        try{
            List<ObLocation> list = new ArrayList<ObLocation>();
            String[] columns = new String[]{
                    FIELDS.ID_LOCATION.name(),
                    FIELDS.LATITUDE.name(),
                    FIELDS.LOGITUDE.name(),
                    FIELDS.CEP.name(),
                    FIELDS.COUNTRY.name(),
                    FIELDS.STATE.name(),
                    FIELDS.CITY.name(),
                    FIELDS.DISTRICT.name()};

            Cursor cursor = this.DB_ENJOY.query(this.TB_LOCATION,columns,null,null,null,null,null);

            if (cursor.getCount()>0){
                cursor.moveToFirst();
                ObLocation obLocation;
                do{
                    obLocation = new ObLocation();
                    obLocation.setID_LOCATION(cursor.getLong(0));
                    obLocation.setLATITUDE(cursor.getDouble(1));
                    obLocation.setLOGITUDE(cursor.getDouble(2));
                    obLocation.setCEP(cursor.getString(3));
                    obLocation.setCOUNTRY(cursor.getString(4));
                    obLocation.setSTATE(cursor.getString(5));
                    obLocation.setCITY(cursor.getString(6));
                    obLocation.setDISTRICT(cursor.getString(7));

                    list.add(obLocation);
                }while (cursor.moveToNext());
            }
            return list;
        }catch (Exception ex){
            return null;
        }
    }
}

