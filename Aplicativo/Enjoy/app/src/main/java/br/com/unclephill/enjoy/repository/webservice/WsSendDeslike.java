package br.com.unclephill.enjoy.repository.webservice;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsDeslike;
import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApSession.DESLIKE_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LIKE_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;

public class WsSendDeslike extends AsyncTask<Void,Void,Void>{
    private Context context;
    private BsDeslike bsDeslike;
    private String URL = URL_BASE + "feed/deslike/";
    private boolean Status;

    public WsSendDeslike(Context context){
        this.context = context;
        this.Status = false;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (!Status){
            Status = true;
            try{
                this.bsDeslike = new BsDeslike(DESLIKE_SESSION);
                this.bsDeslike.setJSON(
                        Connection(URL,this.bsDeslike.getJSON(),
                                ApConnection.TypeConnection.POST));

            }catch (Exception ex){
                DESLIKE_SESSION.setSUCESS(false);
                DESLIKE_SESSION.setMESSAGE(ex.getMessage());
            }finally {
                Status = false;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
    }
}
