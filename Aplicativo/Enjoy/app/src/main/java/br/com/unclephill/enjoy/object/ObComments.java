package br.com.unclephill.enjoy.object;

public class ObComments {
    private long ID_COMMENTS;
    private long ID_PUBLICATION;
    private long ID_USER;
    private String DESCRIPTION;
    private String DATE;
    private String AVATAR;
    private String NAME;
    private String MESSAGE;
    private boolean SUCESS;

    public ObComments(){
        this.ID_COMMENTS = 0;
        this.ID_PUBLICATION = 0;
        this.ID_USER = 0;
        this.DESCRIPTION = "";
        this.DATE = "";
        this.NAME = "";
        this.AVATAR = "";
    }

    public ObComments(long ID_COMMENTS, long ID_PUBLICATION, long ID_USER, String DESCRIPTION, String DATE, String NAME, String AVATAR){
        this.ID_COMMENTS = ID_COMMENTS;
        this.ID_PUBLICATION = ID_PUBLICATION ;
        this.ID_USER = ID_USER;
        this.DESCRIPTION = DESCRIPTION;
        this.DATE = DATE;
        this.NAME = NAME;
        this.AVATAR = AVATAR;
    }

    public long getID_PUBLICATION() {
        return ID_PUBLICATION;
    }

    public void setID_PUBLICATION(long ID_PUBLICATION) {
        this.ID_PUBLICATION = ID_PUBLICATION;
    }

    public long getID_USER() {
        return ID_USER;
    }

    public void setID_USER(long ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public boolean isSUCESS() {
        return SUCESS;
    }

    public void setSUCESS(boolean SUCESS) {
        this.SUCESS = SUCESS;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION.replace("'","''");
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public long getID_COMMENTS() {
        return ID_COMMENTS;
    }

    public void setID_COMMENTS(long ID_COMMENTS) {
        this.ID_COMMENTS = ID_COMMENTS;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getAVATAR() {
        return AVATAR;
    }

    public void setAVATAR(String AVATAR) {
        this.AVATAR = AVATAR;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }
}
