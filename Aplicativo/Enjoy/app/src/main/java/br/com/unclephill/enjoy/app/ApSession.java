package br.com.unclephill.enjoy.app;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.AccessToken;

import java.util.ArrayList;

import br.com.unclephill.enjoy.adapter.AdComments;
import br.com.unclephill.enjoy.adapter.AdPublication;
import br.com.unclephill.enjoy.business.BsMaps;
import br.com.unclephill.enjoy.object.ObComments;
import br.com.unclephill.enjoy.object.ObDeslike;
import br.com.unclephill.enjoy.object.ObLike;
import br.com.unclephill.enjoy.object.ObListComments;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.object.ObLocation;
import br.com.unclephill.enjoy.object.ObPublication;
import br.com.unclephill.enjoy.object.ObUser;
import br.com.unclephill.enjoy.repository.database.TbLocation;
import br.com.unclephill.enjoy.repository.database.TbPublication;
import br.com.unclephill.enjoy.repository.database.TbStart;
import br.com.unclephill.enjoy.repository.database.TbUser;

public class ApSession {
    public static ProgressDialog PG_DIALOG;
    public static LinearLayout LL_UpdateFeed;
    public static LinearLayout LL_Feed;
    public static RelativeLayout LL_ContainerFragment;
    public static RecyclerView RV_Feed;
    public static RecyclerView RV_Comments;
    public static SwipeRefreshLayout SC_SwipeContainer;
    public static Boolean theFirstAcess = false;

    public static String URL_BASE = "http://enjoys.96.lt/rest/index.php/api/";
    public static String DB_NAME = "ENJOYs.sql";
    public static int DB_VERSION = 2;

    public static final String MASCARA_TELEFONE = "(##)####-####";
    public static final String MASCARA_CELULAR = "(##)#####-####";
    public static final String MASCARA_CPF = "###.###.###-##";
    public static final String MASCARA_CNPJ = "##.###.###/####-##";
    public static final String MASCARA_CEP = "##.###-###";
    public static final String MASCARA_DATA = "##/##/####";

    public static int IMAGEM_CAMERA = 0;
    public static int IMAGEM_INTERNA = 1;

    public static ObUser USER_SESSION = new ObUser();
    public static ObLocation LOCATION_SESSION = new ObLocation();
    public static Boolean START_SESSION = false;
    public static ObPublication PUBLICATION_SESSION = new ObPublication();
    public static ObListPublication LIST_PUBLICATION_SESSION = new ObListPublication();
    public static ObListPublication LIST_PUBLICATION = new ObListPublication();
    public static ObListComments LIST_COMMENTS_SESSION = new ObListComments();
    public static ObComments COMMENTS_SESSION = new ObComments();
    public static ObLike LIKE_SESSION = new ObLike();
    public static ObDeslike DESLIKE_SESSION = new ObDeslike();
    public static AccessToken ACESS_TOKEN;


    public static AdPublication ADAPTER_PUBLICATION;
    public static AdComments ADAPTER_COMMENTS;
    public static String SEARCH_FEED;
    public static BsMaps MAPS;

    public static TbUser TB_USER;
    public static TbLocation TB_LOCATION;
    public static TbStart TB_START;
    public static TbPublication TB_PUBLICATION;
}
