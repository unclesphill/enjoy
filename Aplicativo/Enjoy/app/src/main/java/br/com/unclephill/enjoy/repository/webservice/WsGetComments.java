package br.com.unclephill.enjoy.repository.webservice;

import android.content.Context;
import android.os.AsyncTask;

import br.com.unclephill.enjoy.adapter.AdComments;
import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsListComments;

import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.LIST_COMMENTS_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.RV_Comments;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;
import static br.com.unclephill.enjoy.app.ApSession.ADAPTER_COMMENTS;

public class WsGetComments extends AsyncTask<Void,Void,Void> {
    private BsListComments bsListComments;
    private Context context;
    private String URL = URL_BASE + "feed/findcomments/";
    private Boolean Start;
    private long ID_COMMENTS;

    public WsGetComments(Context context,long ID_COMMENTS){
        this.Start = false;
        this.context = context;
        this.ID_COMMENTS = ID_COMMENTS;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        mostrarProgresso(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (!Start){
            Start = true;
            try{
                this.bsListComments = new BsListComments(this.ID_COMMENTS);
                this.bsListComments.setJSON(
                        Connection(
                                URL,this.bsListComments.getJSON(),
                                ApConnection.TypeConnection.POST)
                );
            }catch (Exception ex){
                LIST_COMMENTS_SESSION.setSUCCESS(false);
                LIST_COMMENTS_SESSION.setMESSAGE(ex.getMessage());
            }
            Start = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
        try{
            if (LIST_COMMENTS_SESSION.isSUCCESS()){
                ADAPTER_COMMENTS = new AdComments(context,LIST_COMMENTS_SESSION);
                RV_Comments.setAdapter(ADAPTER_COMMENTS);
            }
        }catch (Exception ex){
            excecoes(context,ex);
        }finally {
            fecharProgresso();
        }
    }
}
