package br.com.unclephill.enjoy.object;

import static br.com.unclephill.enjoy.app.ApFunctions.Quotes;

public class ObPublication {

    private long ID_PUBLICATION;//
    private long ID_USER;//
    private String DESCRIPTION;//
    private String DATE;//
    private String IMAGE;//
    private String COUNTRY;//
    private String STATE;//
    private String CITY;//
    private String DISTRICT;//
    private String CEP;//
    private double LATITUDE;//
    private double LONGITUDE;//
    private long QTD_COMMENTS;//
    private long QTD_DESLIKE;//
    private long QTD_LIKES;//
    private String NAME;//
    private double DISTANCIA;//
    private String AVATAR;////TEM QUE IMPLEMENTAR NA API
    private String LAST_NAME;//// TEM QUE IMPLEMENTAR NA API
    private int ILIKE;
    private int IDESLIKE;
    private String MESSAGE;
    private Boolean SUCESS;

    public void ObPublication(){
        this.ID_PUBLICATION = 0;
        this.ID_USER = 0;
        this.DESCRIPTION = "";
        this.DATE = getDATE();
        this.IMAGE = "";
        this.COUNTRY ="";
        this.CITY="";
        this.DISTRICT ="";
        this.STATE = "";
        this.CEP ="";
        this.LATITUDE=0.0;
        this.LONGITUDE =0.0;
        this.QTD_COMMENTS = 0;
        this.QTD_DESLIKE = 0;
        this.QTD_LIKES = 0;
        this.NAME = "";
        this.DISTANCIA = 0;
        this.ILIKE = 0;
        this.IDESLIKE = 0;
        this.MESSAGE ="";
        this.SUCESS =false;
    }

    public void ObPublication(long ID_PUBLICATION,long ID_USER,String DESCRIPTION, String DATE ,String IMAGE,
                              String COUNTRY,String CITY,String DISTRICT,String STATE,String CEP,double LATITUDE,
                              double LONGITUDE,long QTD_COMMENTS,long QTD_DESLIKE,long QTD_LIKES,String NAME,int DISTANCIA,
                              int ILIKE, int IDESLIKE ,String MESSAGE,Boolean SUCESS){
        this.ID_PUBLICATION = ID_PUBLICATION;
        this.ID_USER = ID_USER;
        this.DESCRIPTION = DESCRIPTION;
        this.DATE = DATE;
        this.IMAGE = IMAGE;
        this.COUNTRY = COUNTRY;
        this.CITY = CITY;
        this.DISTRICT = DISTRICT;
        this.STATE = STATE;
        this.CEP = CEP;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.QTD_COMMENTS = QTD_COMMENTS;
        this.QTD_DESLIKE = QTD_DESLIKE;
        this.QTD_LIKES = QTD_LIKES;
        this.NAME = NAME;
        this.DISTANCIA = DISTANCIA;
        this.ILIKE = ILIKE;
        this.IDESLIKE = IDESLIKE;
        this.MESSAGE = MESSAGE;
        this.SUCESS = SUCESS;
    }

    public long getID_PUBLICATION() {
        return ID_PUBLICATION;
    }

    public void setID_PUBLICATION(long ID_PUBLICATION) {
        this.ID_PUBLICATION = ID_PUBLICATION;
    }

    public long getID_USER() {
        return ID_USER;
    }

    public void setID_USER(long ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getDESCRIPTION() {
        return Quotes(DESCRIPTION);
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getCOUNTRY() {
        return Quotes(COUNTRY);
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getCITY() {
        return Quotes(CITY);
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getDISTRICT() {
        return Quotes(DISTRICT);
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public String getCEP() {
        return Quotes(CEP);
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public double getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(double LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public double getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(double LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getMESSAGE() {
        return Quotes(MESSAGE);
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public Boolean getSUCESS() {
        return SUCESS;
    }

    public void setSUCESS(Boolean SUCESS) {
        this.SUCESS = SUCESS;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getSTATE() {
        return Quotes(STATE);
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public long getQTD_COMMENTS() {
        return QTD_COMMENTS;
    }

    public void setQTD_COMMENTS(long QTD_COMMENTS) {
        this.QTD_COMMENTS = QTD_COMMENTS;
    }

    public long getQTD_DESLIKE() {
        return QTD_DESLIKE;
    }

    public void setQTD_DESLIKE(long QTD_DESLIKE) {
        this.QTD_DESLIKE = QTD_DESLIKE;
    }

    public long getQTD_LIKES() {
        return QTD_LIKES;
    }

    public void setQTD_LIKES(long QTD_LIKES) {
        this.QTD_LIKES = QTD_LIKES;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public double getDISTANCIA() {
        return DISTANCIA;
    }

    public void setDISTANCIA(double DISTANCIA) {
        this.DISTANCIA = DISTANCIA;
    }

    public String getAVATAR() {
        return AVATAR;
    }

    public void setAVATAR(String AVATAR) {
        this.AVATAR = AVATAR;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public int getILIKE() {
        return ILIKE;
    }

    public void setILIKE(int ILIKE) {
        this.ILIKE = ILIKE;
    }

    public int getIDESLIKE() {
        return IDESLIKE;
    }

    public void setIDESLIKE(int IDESLIKE) {
        this.IDESLIKE = IDESLIKE;
    }
}
