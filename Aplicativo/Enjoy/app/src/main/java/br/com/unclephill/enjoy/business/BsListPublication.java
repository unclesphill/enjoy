package br.com.unclephill.enjoy.business;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.adapter.AdPublication;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.object.ObPublication;
import br.com.unclephill.enjoy.view.activity.VwAcFeed;

import static br.com.unclephill.enjoy.app.ApSession.ADAPTER_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.SEARCH_FEED;
import static br.com.unclephill.enjoy.app.ApSession.TB_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class BsListPublication {
    private ObListPublication obListPublication;
    private Context context;

    public BsListPublication(ObListPublication obListPublication, Context context){
        this.obListPublication = obListPublication;
        this.context = context;
    }

    public void setJSON(JSONObject json){
        Gson gson = new Gson();
        SEARCH_FEED = "";
        LIST_PUBLICATION = gson.fromJson(json.toString(),ObListPublication.class);
        if (!LIST_PUBLICATION.isSUCCESS() &&
                LIST_PUBLICATION.getMESSAGE().equals("Não há eventos proximo a sua localização.")){
            LIST_PUBLICATION.setINDEX(-1);
        }
    }

    public JSONObject getJSON() throws JSONException {
        try{
            JSONObject json = new JSONObject();

            json.put("INDEX",LIST_PUBLICATION.getINDEX());
            json.put("LATITUDE",LOCATION_SESSION.getLATITUDE());
            json.put("LONGITUDE",LOCATION_SESSION.getLOGITUDE());
            json.put("ID_USER",USER_SESSION.getID_USER());
            json.put("DESCRIPTION",SEARCH_FEED == "" ? null : SEARCH_FEED);

            return json;
        }catch (Exception ex){
            throw  ex;
        }
    }
}
