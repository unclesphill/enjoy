package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApFunctions;
import br.com.unclephill.enjoy.repository.webservice.WsGetComments;
import br.com.unclephill.enjoy.repository.webservice.WsSendComments;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.COMMENTS_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.RV_Comments;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class VwAcComments extends Activity implements ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack {

    private ImageButton idBtnSendComment;
    private EditText idEdtComments;
    private RecyclerView idRvComments;
    private long ID_PUBLICATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.vw_ac_comments);
        this.inflar();
    }

    public void inflar(){
        this.idBtnSendComment = (ImageButton) findViewById(R.id.idBtnSendComment);
        this.idEdtComments = (EditText) findViewById(R.id.idEdtComments);
        this.idRvComments = (RecyclerView) findViewById(R.id.idRvComments);
        this.idRvComments.setHasFixedSize(true);
        this.idRvComments.addOnItemTouchListener(new ApFunctions.RecyclerViewTouchListener( VwAcComments.this, this.idRvComments, this ));
        LinearLayoutManager llm = new LinearLayoutManager(VwAcComments.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.idRvComments.setLayoutManager(llm);
        RV_Comments = this.idRvComments;

        Intent intent = getIntent();
        this.ID_PUBLICATION = intent.getExtras().getLong("ID_PUBLICATION");
        try{
            new WsGetComments(VwAcComments.this,this.ID_PUBLICATION).execute();
        }catch (Exception ex){
            excecoes(VwAcComments.this,ex);
        }
    }

    public void onClickSendComment(View view){
        try{
            if (!this.validaView()){return;}
            COMMENTS_SESSION.setID_USER(USER_SESSION.getID_USER());
            COMMENTS_SESSION.setID_PUBLICATION(this.ID_PUBLICATION);
            COMMENTS_SESSION.setDESCRIPTION(this.idEdtComments.getText().toString());
            new WsSendComments(VwAcComments.this).execute();
        }catch (Exception ex){
            excecoes(VwAcComments.this,ex);
        }
    }

    public boolean validaView(){
        if (this.idEdtComments.getText().toString().equals("")){
            modalNeutro(VwAcComments.this,"Atenção!","Informe um comentário!","OK");
            this.idEdtComments.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onClickListener(View view, int position) {

    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }
}
