package br.com.unclephill.enjoy.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import br.com.unclephill.enjoy.adapter.AdPublication;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.object.ObLocation;
import br.com.unclephill.enjoy.object.ObUser;
import br.com.unclephill.enjoy.repository.database.TbLocation;
import br.com.unclephill.enjoy.repository.database.TbPublication;
import br.com.unclephill.enjoy.repository.database.TbUser;
import android.util.Base64;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import static br.com.unclephill.enjoy.app.ApSession.ACESS_TOKEN;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.LIST_PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LL_ContainerFragment;
import static br.com.unclephill.enjoy.app.ApSession.LL_Feed;
import static br.com.unclephill.enjoy.app.ApSession.LL_UpdateFeed;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.PG_DIALOG;
import static br.com.unclephill.enjoy.app.ApSession.RV_Feed;
import static br.com.unclephill.enjoy.app.ApSession.SC_SwipeContainer;
import static br.com.unclephill.enjoy.app.ApSession.TB_LOCATION;
import static br.com.unclephill.enjoy.app.ApSession.TB_PUBLICATION;
import static br.com.unclephill.enjoy.app.ApSession.TB_USER;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.ADAPTER_PUBLICATION;

public class ApFunctions {

    public static void loadPublication(Context context){
        try{
            if (LIST_PUBLICATION.isSUCCESS()){
                ADAPTER_PUBLICATION = new AdPublication(context, LIST_PUBLICATION);
                AdPublication adapter = (AdPublication) RV_Feed.getAdapter();
                if (LIST_PUBLICATION.getINDEX() <= 10){
                    LIST_PUBLICATION_SESSION = new ObListPublication();
                    RV_Feed.setAdapter(ADAPTER_PUBLICATION);
                }else {
                    for (int x = 0; x < LIST_PUBLICATION.getLISTA().size(); x++) {
                        adapter.addListItem(LIST_PUBLICATION.getLISTA().get(x));
                    }
                }
                TB_PUBLICATION.deleteAll();
                for (int x = 0; x < LIST_PUBLICATION.getLISTA().size(); x++) {
                    TB_PUBLICATION.insert(LIST_PUBLICATION.getLISTA(x));
                    LIST_PUBLICATION_SESSION.setLISTA(LIST_PUBLICATION.getLISTA().get(x));
                }
                LL_Feed.removeView(LL_UpdateFeed);
                ViewGroup.LayoutParams params;
                params = LL_ContainerFragment.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                LL_ContainerFragment.setLayoutParams(params);
                params = SC_SwipeContainer.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                SC_SwipeContainer.setLayoutParams(params);
            }else{
                Toast.makeText(context,LIST_PUBLICATION.getMESSAGE(),Toast.LENGTH_LONG).show();
            }
        }catch (Exception ex){
            throw  ex;
        }finally {
            SC_SwipeContainer.setRefreshing(false);
        }
    }

    public static void load(Context context){
        try{
            if (LIST_PUBLICATION.isSUCCESS()){
                TB_PUBLICATION.deleteAll();
                for (int x = 0; x < LIST_PUBLICATION.getLISTA().size(); x++) {
                    TB_PUBLICATION.insert(LIST_PUBLICATION.getLISTA(x));
                    LIST_PUBLICATION_SESSION.setLISTA(LIST_PUBLICATION.getLISTA().get(x));
                }

                ADAPTER_PUBLICATION = new AdPublication(context, LIST_PUBLICATION_SESSION);

                if (LIST_PUBLICATION.getINDEX() <= 10){
                    LIST_PUBLICATION_SESSION = new ObListPublication();
                    RV_Feed.setAdapter(ADAPTER_PUBLICATION);
                }else {
                    RV_Feed.setAdapter(ADAPTER_PUBLICATION);
                }

                LL_Feed.removeView(LL_UpdateFeed);
                ViewGroup.LayoutParams params;
                params = LL_ContainerFragment.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                LL_ContainerFragment.setLayoutParams(params);
                params = SC_SwipeContainer.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                SC_SwipeContainer.setLayoutParams(params);
            }else{
                Toast.makeText(context,LIST_PUBLICATION.getMESSAGE(),Toast.LENGTH_LONG).show();
            }
        }catch (Exception ex){
            throw  ex;
        }finally {
            SC_SwipeContainer.setRefreshing(false);
        }
    }

    public static void iniciarActivity(Context context, Class classe, Bundle paramentros){
        Intent intent = new Intent(context,classe);
        if (paramentros != null){intent.putExtras(paramentros);}
        context.startActivity(intent);
    }

    public static void fecharActivity(Context context){
        ((Activity) context).finish();
    }

    public static void mostrarProgresso(Context context){
        PG_DIALOG = new ProgressDialog(context);
        PG_DIALOG.setMessage("Aguarde...");
        PG_DIALOG.setIndeterminate(false);
        PG_DIALOG.setCancelable(false);
        PG_DIALOG.setProgress(0);
        PG_DIALOG.show();
    }

    public static double milhaParseKm(double milha){
        return Math.round((milha * 1.60934)/1);
    }

    public static double kmParseMilha(double km){
        return Math.round((km * 0.621371)/1);
    }

    public static void fecharProgresso(){
        if (PG_DIALOG.isShowing()){ PG_DIALOG.dismiss();}
    }

    public static void iniciarFragment(Fragment fragment , int id, FragmentManager fragmentManager){
        FragmentManager manFrg = fragmentManager;
        FragmentTransaction traFrg = manFrg.beginTransaction();
        traFrg.replace(id, fragment);
        traFrg.setTransition(FragmentTransaction.TRANSIT_NONE);
        traFrg.commit();
    }

    public static void fecharFragment(Fragment fragment, int id, FragmentManager fragmentManager){
        FragmentManager manFrg = fragmentManager;
        FragmentTransaction traFrg = manFrg.beginTransaction();
        traFrg.remove(fragment);
        traFrg.setTransition(FragmentTransaction.TRANSIT_NONE);
        traFrg.commit();
    }

    public static AlertDialog modalNeutro(Context context, String titulo, String menssagem, String mensagemBotao){
        return new AlertDialog.Builder(context)
                .setTitle(titulo)
                .setMessage(menssagem)
                .setNeutralButton(mensagemBotao, null).show();
    }

    public static void excecoes(Context context,Exception erro){
        new AlertDialog.Builder(context)
                .setTitle("Atenção!")
                .setMessage("Erro: " + erro.getMessage() + "  " + erro.getStackTrace())
                .setNeutralButton("OK", null).show();
    }

    public static String imageParseBase64(Bitmap bpm){
        try{
            if (bpm == null){return "";}
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bpm.compress(Bitmap.CompressFormat.JPEG,70, stream);
            byte[] byteArray = stream.toByteArray();
            String base64 = Base64.encodeToString(byteArray,Base64.NO_WRAP);
            return "data:image/jpg;base64," + base64;
        }catch (Exception ex){return null;}
    }

    public static Bitmap base64ParseImage(String b64){
        try{
            if(b64==""){return null;}
            byte[] byteArray = Base64.decode(b64,Base64.DEFAULT);
            Bitmap bpm = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
            return bpm;
        }catch (Exception ex){return null;}
    }

    public static byte[] bitmapParseByte(Bitmap bmp){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap byteParseBitmap(byte[] bites){
        return BitmapFactory.decodeByteArray(bites, 0, bites.length);
    }

    public static Date getCurrentDate(){
        DateFormat formatter = new SimpleDateFormat();
        return formatter.getCalendar().getTime();
    }

    public static String formatDate(String date){
        try{
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date data = format.parse(date);
            format.applyPattern("dd-MM-yyyy HH:mm:ss");
            return format.format(data);
        }catch (Exception ex){
             return "";
        }
    }

    public static Date parseDate(String myDate){
        Date date = new Date();
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date = (Date)formatter.parse(myDate);
            return date;
        } catch (ParseException e) {}
        return date;
    }

    public static void logOut(Context context){
        TB_USER = new TbUser(context);
        TB_LOCATION = new TbLocation(context);
        if (TB_USER.select().size() > 0){TB_USER.deleteAll();}
        if (LOCATION_SESSION.getID_LOCATION()>0){TB_LOCATION.delete(LOCATION_SESSION);}
        USER_SESSION = new ObUser();
        LOCATION_SESSION = new ObLocation();
        TB_PUBLICATION.deleteAll();
        TB_PUBLICATION = new TbPublication(context);
        LIST_PUBLICATION = new ObListPublication();
        LIST_PUBLICATION_SESSION = new ObListPublication();
        if (ACESS_TOKEN != null){
            if (!ACESS_TOKEN.isExpired()){ LoginManager.getInstance().logOut();}
        }
    }

    public static void aplicarMascaras(EditText edt, String mascara){
        MaskEditTextChangedListener campo = new MaskEditTextChangedListener(mascara,edt);
        edt.addTextChangedListener(campo);
    }

    public static String Quotes(String str){
        if (str != null){
            return str.replace("'","''");
        }else{
            return "";
        }
    }

    public static Bitmap getRoundedCornerBitmap(Context context, Bitmap input,
                                                int pixels , int w , int h ,
                                                boolean squareTL, boolean squareTR,
                                                boolean squareBL, boolean squareBR  ) {

        Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, w, h);
        final RectF rectF = new RectF(rect);

        final float roundPx = pixels*densityMultiplier;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        if (squareTL ){
            canvas.drawRect(0, 0, w/2, h/2, paint);
        }
        if (squareTR ){
            canvas.drawRect(w/2, 0, w, h/2, paint);
        }
        if (squareBL ){
            canvas.drawRect(0, h/2, w/2, h, paint);
        }
        if (squareBR ){
            canvas.drawRect(w/2, h/2, w, h, paint);
        }

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, 0,0, paint);

        return output;
    }

    public static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerHack rvoclh){
            mContext = c;
            mRecyclerViewOnClickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onLongPressClickListener(cv,
                                rv.getChildPosition(cv) );
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onClickListener(cv,
                                rv.getChildPosition(cv) );
                    }

                    return(true);
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        public interface RecyclerViewOnClickListenerHack {
            public void onClickListener(View view, int position);
            public void onLongPressClickListener(View view, int position);
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
