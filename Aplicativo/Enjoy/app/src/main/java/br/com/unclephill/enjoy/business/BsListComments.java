package br.com.unclephill.enjoy.business;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.unclephill.enjoy.object.ObListComments;
import static br.com.unclephill.enjoy.app.ApSession.COMMENTS_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LIST_COMMENTS_SESSION;

public class BsListComments {
    private long ID_COMMENTS;
    public BsListComments(long ID_COMMENTS){
        this.ID_COMMENTS = ID_COMMENTS;
    }

    public void setJSON(JSONObject json){
        Gson gson = new Gson();
        LIST_COMMENTS_SESSION = gson.fromJson(json.toString(),ObListComments.class);
    }

    public JSONObject getJSON() throws JSONException {
        try{
            JSONObject json = new JSONObject();

            json.put("ID_PUBLICATION",this.ID_COMMENTS);

            return json;
        }catch (Exception ex){
            throw  ex;
        }
    }

}
