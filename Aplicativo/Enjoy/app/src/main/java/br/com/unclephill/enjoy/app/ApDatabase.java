package br.com.unclephill.enjoy.app;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.ExecutionException;

import static br.com.unclephill.enjoy.app.ApSession.DB_NAME;
import static br.com.unclephill.enjoy.app.ApSession.DB_VERSION;

public class ApDatabase extends SQLiteOpenHelper {
    public ApDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase DbEnjoy) {
        try {
            String SQL;

            SQL =  " CREATE TABLE START( ";
            SQL += "    ID_START INTEGER PRIMARY KEY AUTOINCREMENT , ";
            SQL += "    START INTEGER NOT NULL ";
            SQL += " ); ";
            DbEnjoy.execSQL(SQL);

            SQL =  " CREATE TABLE USER( ";
            SQL += "    ID_USER INTEGER PRIMARY KEY , ";
            SQL += "    NAME TEXT NOT NULL , ";
            SQL += "    LAST_NAME TEXT NOT NULL , ";
            SQL += "    EMAIL TEXT NOT NULL , ";
            SQL += "    PHONE TEXT NOT NULL , ";
            SQL += "    PASSWORD TEXT NOT NULL , ";
            SQL += "    AVATAR TEXT NOT NULL ";
            SQL += " ); ";
            DbEnjoy.execSQL(SQL);

            SQL =  " CREATE TABLE LOCATION( ";
            SQL += "    ID_LOCATION INTEGER PRIMARY KEY AUTOINCREMENT , ";
            SQL += "    LATITUDE FLOAT NULL , ";
            SQL += "    LOGITUDE FLOAT NULL , ";
            SQL += "    CEP TEXT NULL , ";
            SQL += "    COUNTRY TEXT NULL , ";
            SQL += "    STATE TEXT NULL , ";
            SQL += "    CITY TEXT NULL , ";
            SQL += "    DISTRICT TEXT NULL ";
            SQL += " ); ";
            DbEnjoy.execSQL(SQL);

            SQL =  " CREATE TABLE PUBLICATION( ";
            SQL += "    ID_PUBLICATION INTEGER PRIMARY KEY, ";
            SQL += "    ID_USER BIGINT NOT NULL, ";
            SQL += "    DESCRIPTION TEXT NULL, ";
            SQL += "    LOCATION TEXT NULL, ";
            SQL += "    DATE DATETIME NOT NULL, ";
            SQL += "    IMAGE TEXT NULL, ";
            SQL += "    COUNTRY TEXT NULL, ";
            SQL += "    STATE TEXT NULL, ";
            SQL += "    CITY TEXT NULL, ";
            SQL += "    DISTRICT TEXT NULL, ";
            SQL += "    CEP TEXT NULL, ";
            SQL += "    LATITUDE FLOAT NOT NULL, ";
            SQL += "    LONGITUDE FLOAT NOT NULL, ";
            SQL += "    NAME TEXT NULL,";
            SQL += "    LAST_NAME TEXT NULL,";
            SQL += "    AVATAR TEXT NULL,";
            SQL += "    DISTANCIA INTEGER NULL,";
            SQL += "    QTD_COMMENTS INTEGER NULL,";
            SQL += "    QTD_DESLIKE INTEGER NULL,";
            SQL += "    QTD_LIKES INTEGER NULL,";
            SQL += "    ILIKE INTEGER NULL,";
            SQL += "    IDESLIKE INTEGER NULL";
            SQL += " ); ";
            DbEnjoy.execSQL(SQL);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase DbEnjoy, int i, int i1) {
        try {
            String SQL;

            SQL  = " DROP TABLE START; ";
            DbEnjoy.execSQL(SQL);

            SQL = " DROP TABLE USER; ";
            DbEnjoy.execSQL(SQL);

            SQL = " DROP TABLE LOCATION; ";
            DbEnjoy.execSQL(SQL);

            SQL = " DROP TABLE PUBLICATION; ";
            DbEnjoy.execSQL(SQL);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
