package br.com.unclephill.enjoy.object;

import java.util.ArrayList;

import static br.com.unclephill.enjoy.app.ApFunctions.Quotes;

public class ObListPublication {
    private ArrayList<ObPublication> LISTA;
    private long INDEX;
    private Boolean SUCCESS;
    private String MESSAGE;

    public ObListPublication(){
        this.setLISTA(new ArrayList<ObPublication>());
        this.setINDEX(0);
        this.setSUCCESS(false);
        this.setMESSAGE("");
    }

    public ObListPublication(ArrayList<ObPublication> LIST_PUBLICATION){
        this.setLISTA(LIST_PUBLICATION);
        this.setINDEX(0);
        this.setSUCCESS(false);
        this.setMESSAGE("");
    }

    public ArrayList<ObPublication> getLISTA() {
        return LISTA;
    }

    public ObPublication getLISTA(int index){
        return LISTA.get(index);
    }

    public void setLISTA(ArrayList<ObPublication> LISTA) {
        this.LISTA = LISTA;
    }

    public void setLISTA(ObPublication PUBLICATION) {
        this.LISTA.add(PUBLICATION);
    }

    public Boolean isSUCCESS() {
        return SUCCESS;
    }

    public void setSUCCESS(Boolean SUCCESS) {
        this.SUCCESS = SUCCESS;
    }

    public String getMESSAGE() {
        return Quotes(MESSAGE);
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public long getINDEX() {
        return INDEX;
    }

    public void setINDEX(long INDEX) {
        this.INDEX = INDEX;
    }
}
