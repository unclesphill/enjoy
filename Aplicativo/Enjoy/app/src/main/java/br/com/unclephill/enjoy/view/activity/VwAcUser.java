package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.repository.webservice.WsNewUser;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.unclephill.enjoy.app.ApFunctions.aplicarMascaras;
import static br.com.unclephill.enjoy.app.ApFunctions.base64ParseImage;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.imageParseBase64;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.IMAGEM_CAMERA;
import static br.com.unclephill.enjoy.app.ApSession.IMAGEM_INTERNA;
import static br.com.unclephill.enjoy.app.ApSession.MASCARA_CELULAR;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class VwAcUser extends Activity {
    private EditText idEdtName;
    private EditText idEdtLastName;
    private EditText idEdtEmail;
    private EditText idEdtPassword;
    private EditText idEdtTelephone;
    private Button idBtSave;
    private CheckBox idCkAceept;
    private TextView idTxwTerms;
    private CircleImageView idImgPhoto;
    private ImageButton idImgArrow;
    private Boolean bTakePhoto = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.inflar();
        this.loadUser();
    }

    private void inflar(){
        this.setContentView(R.layout.vw_ac_user);
        this.idEdtName = (EditText) findViewById(R.id.idEdtName);
        this.idEdtLastName = (EditText) findViewById(R.id.idEdtLastName);
        this.idEdtEmail = (EditText) findViewById(R.id.idEdtEmail);
        this.idEdtPassword = (EditText) findViewById(R.id.idEdtPassword);
        this.idEdtTelephone = (EditText) findViewById(R.id.idEdtTelefone);
        this.idBtSave = (Button) findViewById(R.id.idBtSave);
        this.idCkAceept = (CheckBox) findViewById(R.id.idCkAceppt);
        this.idTxwTerms = (TextView) findViewById(R.id.idTxwTerms);
        this.idImgPhoto = (CircleImageView) findViewById(R.id.idImgPhoto);
        this.idTxwTerms = (TextView) findViewById(R.id.idTxwTerms);
        this.idImgArrow = (ImageButton) findViewById(R.id.idImgArrow);
        aplicarMascaras(this.idEdtTelephone, MASCARA_CELULAR);
    }

    public void loadUser(){
        if (USER_SESSION.getID_USER() <= 0){return;}
        Bitmap bmp = base64ParseImage(USER_SESSION.getAVATAR().replace("data:image/jpg;base64,",""));
        if (bmp!=null){
            this.idImgPhoto.setImageBitmap(bmp);
            this.idImgPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }else {
            if (!USER_SESSION.getAVATAR().equals("")) {
                Picasso.with(VwAcUser.this).load(USER_SESSION.getAVATAR()).into(this.idImgPhoto);
                this.idImgPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
                this.bTakePhoto = true;
            }
        }
        this.idEdtName.setText(USER_SESSION.getNAME());
        this.idEdtLastName.setText(USER_SESSION.getLAST_NAME());
        this.idEdtTelephone.setText(USER_SESSION.getPHONE());
        this.idEdtEmail.setText(USER_SESSION.getEMAIL());
        this.idEdtPassword.setText(USER_SESSION.getPASSWORD());
        this.idEdtEmail.setEnabled(false);
        this.idCkAceept.setVisibility(View.INVISIBLE);
        this.idTxwTerms.setVisibility(View.INVISIBLE);
    }

    public void onClickTakePhoto(View view) {
        AlertDialog alert;
        ArrayList<String> itens = new ArrayList<String>();

        itens.add("Tirar foto");
        itens.add("Escolher da galeria");

        ArrayAdapter adapter = new ArrayAdapter(VwAcUser.this, R.layout.vw_it_item_list, itens);
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(VwAcUser.this);
        builder.setSingleChoiceItems(adapter, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idOpcao) {
                Intent intent;
                switch (idOpcao){
                    case 0: //Tirar foto:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(VwAcUser.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(VwAcUser.this, android.Manifest.permission.CAMERA)) {
                                    ActivityCompat.requestPermissions(VwAcUser.this, new String[]{android.Manifest.permission.CAMERA}, 0);
                                } else {
                                    ActivityCompat.requestPermissions(VwAcUser.this, new String[]{android.Manifest.permission.CAMERA}, 0);
                                }
                            } else {
                                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, IMAGEM_CAMERA);
                                dialog.cancel();
                            }
                        }
                        break;
                    case 1://Pegar da galeria
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(VwAcUser.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(VwAcUser.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                    ActivityCompat.requestPermissions(VwAcUser.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                } else {
                                    ActivityCompat.requestPermissions(VwAcUser.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                }
                            } else {
                                intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType("image/*");
                                startActivityForResult(intent,IMAGEM_INTERNA);
                                dialog.cancel();
                            }
                        }
                        break;
                }
            }
        });
        alert = builder.create();
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if (resultCode == RESULT_OK){
                Bitmap bpm = null;
                if (requestCode == IMAGEM_INTERNA){
                    Uri imagemSelecionada = data.getData();
                    String[] colunas = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(imagemSelecionada, colunas, null, null, null);
                    cursor.moveToFirst();

                    int indexColuna = cursor.getColumnIndex(colunas[0]);
                    String pathImg = cursor.getString(indexColuna);
                    cursor.close();

                    bpm = BitmapFactory.decodeFile(pathImg);
                    this.idImgPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    this.idImgPhoto.setImageBitmap(bpm);

                }else if(requestCode == IMAGEM_CAMERA){
                    bpm = (Bitmap) data.getExtras().get("data");
                    this.idImgPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    this.idImgPhoto.setImageBitmap(bpm);
                }
                USER_SESSION.setAVATAR(imageParseBase64(bpm));
                this.bTakePhoto = true;
            }
        }catch (Exception ex){
            excecoes(this,ex);
        }
    }

    public void onClickSalvar(View view){
        if (!this.validaView()){ return;}
        USER_SESSION.setNAME(this.idEdtName.getText().toString());
        USER_SESSION.setLAST_NAME(this.idEdtLastName.getText().toString());
        USER_SESSION.setEMAIL(this.idEdtEmail.getText().toString());
        USER_SESSION.setPASSWORD(this.idEdtPassword.getText().toString());
        USER_SESSION.setPHONE(this.idEdtTelephone.getText().toString());
        new WsNewUser(VwAcUser.this).execute();
    }

    public void onClickCallBack(View view){
        fecharActivity(VwAcUser.this);
    }

    private Boolean validaView(){
        if (this.idEdtName.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Informe o nome.","OK");
            this.idEdtName.requestFocus();
            return false;
        }

        if(this.idEdtLastName.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Informe o sobrenome.","OK");
            this.idEdtLastName.requestFocus();
            return false;
        }

        if(this.idEdtEmail.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Informe o email","OK");
            this.idEdtEmail.requestFocus();
            return false;
        }

        if(this.idEdtPassword.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Informe a senha.","OK");
            this.idEdtPassword.requestFocus();
            return false;
        }

        if(this.idEdtTelephone.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Informe o telefone.","OK");
            this.idEdtTelephone.requestFocus();
            return false;
        }

        if (!this.idCkAceept.isChecked()){
            modalNeutro(this,"Atenção!","É necessário aceitar os termos de uso!","OK");
            this.idCkAceept.requestFocus();
            return false;
        }

        if (!this.bTakePhoto){
            modalNeutro(this,"Atenção!","É necessário uma foto para o perfil!","OK");
            this.idImgPhoto.requestFocus();
            return false;
        }

        return true;
    }
}
