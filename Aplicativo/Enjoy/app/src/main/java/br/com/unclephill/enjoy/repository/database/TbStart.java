package br.com.unclephill.enjoy.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.unclephill.enjoy.app.ApDatabase;
import br.com.unclephill.enjoy.object.ObLocation;

public class TbStart {
    private SQLiteDatabase DB_ENJOY;
    private String TB_START = "START";

    private enum FIELDS{
        ID_START,
        START
    }

    public TbStart(Context context){
        ApDatabase database = new ApDatabase(context);
        this.DB_ENJOY = database.getWritableDatabase();
    }

    public long insert(Boolean start){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(TbStart.FIELDS.START.name(),(start) ? 1 : 0);

            return this.DB_ENJOY.insert(this.TB_START,null,contentValues);
        }catch (Exception e){
            return 0;
        }
    }


    public Boolean select(){
        Boolean start = false;
        try{
            List<ObLocation> list = new ArrayList<ObLocation>();

            String[] columns = new String[]{
                    TbStart.FIELDS.START.name()};

            Cursor cursor = this.DB_ENJOY.query(this.TB_START,columns,null,null,null,null,null);

            if (cursor.getCount()>0){
                cursor.moveToFirst();

                do{
                    start = (cursor.getInt(0) == 1) ? true:false;
                }while (cursor.moveToNext());
            }
            return start;
        }catch (Exception ex){
            return false;
        }
    }
}
