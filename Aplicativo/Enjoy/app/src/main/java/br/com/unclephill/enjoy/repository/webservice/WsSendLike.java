package br.com.unclephill.enjoy.repository.webservice;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsLike;
import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApSession.LIKE_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;

public class WsSendLike extends AsyncTask<Void,Void,Void> {

    private Context context;
    private BsLike bsLike;
    private String URL = URL_BASE + "feed/like/";
    private boolean Status;

    public WsSendLike(Context context){
        this.context = context;
        this.Status = false;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
            if (!Status){
                Status = true;
                try{
                    this.bsLike = new BsLike(LIKE_SESSION);
                    this.bsLike.setJSON(
                            Connection(URL,this.bsLike.getJSON(),
                                    ApConnection.TypeConnection.POST));

                }catch (Exception ex){
                    LIKE_SESSION.setSUCESS(false);
                    LIKE_SESSION.setMESSAGE(ex.getMessage());
                }finally {
                    Status = false;
                }
            }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
    }
}
