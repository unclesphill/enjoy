package br.com.unclephill.enjoy.object;

public class ObDeslike {
    private long ID_PUBLICATION;
    private long ID_USER;
    private String MESSAGE;
    private boolean SUCESS;

    public ObDeslike(){
        this.setID_PUBLICATION(0);
        this.setID_USER(0);
    }

    public ObDeslike(long ID_PUBLICATION, long ID_USER){
        this.setID_PUBLICATION(ID_PUBLICATION);
        this.setID_USER(ID_USER);
    }

    public long getID_PUBLICATION() {
        return ID_PUBLICATION;
    }

    public void setID_PUBLICATION(long ID_PUBLICATION) {
        this.ID_PUBLICATION = ID_PUBLICATION;
    }

    public long getID_USER() {
        return ID_USER;
    }

    public void setID_USER(long ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public boolean isSUCESS() {
        return SUCESS;
    }

    public void setSUCESS(boolean SUCESS) {
        this.SUCESS = SUCESS;
    }
}
