package br.com.unclephill.enjoy.business;

import org.json.JSONException;
import org.json.JSONObject;
import br.com.unclephill.enjoy.object.ObLike;

public class BsLike {
    private ObLike obLike;

    public BsLike(ObLike obLike){
        this.obLike = obLike;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();
        if (this.obLike.getID_USER() > 0){
            json.put("ID_USER",this.obLike.getID_USER());
        }
        if(this.obLike.getID_PUBLICATION() > 0){
            json.put("ID_PUBLICATION",this.obLike.getID_PUBLICATION());
        }
        return json;
    }

    public void setJSON(JSONObject json) throws JSONException {
        try{
            if (json.toString().contains("ID_USER")){this.obLike.setID_USER(json.getInt("ID_USER"));}
            if (json.toString().contains("ID_PUBLICATION")){this.obLike.setID_PUBLICATION(json.getInt("ID_PUBLICATION"));}
            if (json.toString().contains("SUCCESS")){this.obLike.setSUCESS(json.getBoolean("SUCCESS"));}
            if (json.toString().contains("MESSAGE")){this.obLike.setMESSAGE(json.getString("MESSAGE"));}
        }catch (Exception ex){
            throw  ex;
        }
    }
}
