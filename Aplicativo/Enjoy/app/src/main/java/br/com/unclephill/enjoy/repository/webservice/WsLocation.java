package br.com.unclephill.enjoy.repository.webservice;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.EditText;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.view.activity.VwAcLocation;

import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.MAPS;
import static br.com.unclephill.enjoy.app.ApSession.TB_LOCATION;

public class WsLocation extends AsyncTask<Void,Void,Void> {

    private Context context;
    private Type type;
    EditText idEdtCountry;
    EditText idEdtState;
    EditText idEdtCity ;
    EditText idEdtCEP;

    public enum Type{
        LOCATION,
        ADDRESS
    }

    public WsLocation(Context context,Type type){
        this.context = context;
        this.type = type;
        this.idEdtCountry = (EditText) ((Activity) context).findViewById(R.id.idEdtCountry);
        this.idEdtState = (EditText) ((Activity) context).findViewById(R.id.idEdtState);
        this.idEdtCity = (EditText) ((Activity) context).findViewById(R.id.idEdtCity);
        this.idEdtCEP = (EditText) ((Activity) context).findViewById(R.id.idEdtCEP);
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        mostrarProgresso(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try{
            if (this.type == Type.ADDRESS){
                MAPS.getAddress();
            }else{
                MAPS.getLocation();
            }
        }catch (Exception ex){
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
        try{
            if (this.type == Type.ADDRESS){
                if (LOCATION_SESSION.getID_LOCATION() > 0){
                    TB_LOCATION.update(LOCATION_SESSION);
                }else{
                    LOCATION_SESSION.setID_LOCATION(TB_LOCATION.insert(LOCATION_SESSION));
                }
                modalNeutro(context,"Atenção!","Localização salva!","OK");
            }else{
                this.idEdtCountry.setText(LOCATION_SESSION.getCOUNTRY());
                this.idEdtState.setText(LOCATION_SESSION.getSTATE());
                this.idEdtCity.setText(LOCATION_SESSION.getCITY());
                this.idEdtCEP.setText(LOCATION_SESSION.getCEP());
            }
        }catch (Exception ex){
            excecoes(context,ex);
        }finally {
            fecharProgresso();
        }
    }
}
