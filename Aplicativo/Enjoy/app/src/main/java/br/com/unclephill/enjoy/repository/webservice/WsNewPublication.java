package br.com.unclephill.enjoy.repository.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsPublication;

import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;

public class WsNewPublication extends AsyncTask<Void,Void,Void> {

    private Context context;
    private BsPublication bsPublication;
    private String URL = URL_BASE + "feed/addpublication/";

    public WsNewPublication(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        mostrarProgresso(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try{
            this.bsPublication = new BsPublication(PUBLICATION_SESSION);
            this.bsPublication.setJSON(
                    Connection(URL,this.bsPublication.getJSON(),
                            ApConnection.TypeConnection.POST));
        }catch (Exception ex){
            PUBLICATION_SESSION.setSUCESS(false);
            PUBLICATION_SESSION.setMESSAGE(ex.getMessage());
        }
        return null;
    }
}
