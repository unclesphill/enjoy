package br.com.unclephill.enjoy.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApFunctions;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.object.ObPublication;
import br.com.unclephill.enjoy.repository.webservice.WsSendDeslike;
import br.com.unclephill.enjoy.repository.webservice.WsSendLike;
import br.com.unclephill.enjoy.view.activity.VwAcComments;
import br.com.unclephill.enjoy.view.activity.VwAcShowImageActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import static br.com.unclephill.enjoy.app.ApFunctions.base64ParseImage;
import static br.com.unclephill.enjoy.app.ApFunctions.bitmapParseByte;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.milhaParseKm;
import static br.com.unclephill.enjoy.app.ApSession.DESLIKE_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.LIKE_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class AdPublication extends RecyclerView.Adapter<AdPublication.MyViewHolder> {
    private Context context;
    private ObListPublication obListPublication;
    private LayoutInflater layoutInflater;
    private ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack recyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;

    public AdPublication(Context context, ObListPublication obListPublication){
        this.context = context;
        this.obListPublication = obListPublication;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.scale = this.context.getResources().getDisplayMetrics().density;
        this.width = this.context.getResources().getDisplayMetrics().widthPixels - (int)(14 * this.scale + 0.5f);
        this.height = (this.width / 16) * 9;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = this.layoutInflater.inflate(R.layout.vw_lt_item_feed, viewGroup,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ObPublication item = obListPublication.getLISTA(holder.getLayoutPosition());
        if (holder.itemView.getTag() == null) {
            holder.itemView.setTag(holder);
            holder.ïdNameProfile.setText(item.getNAME() + " " + item.getLAST_NAME());
            holder.idLocationPublication.setText(milhaParseKm(item.getDISTANCIA()) + " Km em " + item.getCITY() + " - " + item.getSTATE());
            holder.idMessagePublication.setText(item.getDESCRIPTION());
            holder.idLikesPublication.setText(item.getQTD_LIKES() + " curtidas");
            holder.idDeslikePulication.setText(item.getQTD_DESLIKE() + " não curtidas");
            holder.idCommentsPulication.setText(item.getQTD_COMMENTS() + " comentários");
            holder.idTxwData.setText(item.getDATE());

            if (item.getILIKE() == 1) {
                holder.idLinearLike.setEnabled(false);
                holder.idLinearDeslike.setEnabled(false);
                holder.idTxwCurti.setTextColor(Color.parseColor("#FFAA00"));
                holder.idAddLikePulication.setBackgroundTintList(context.getResources().getColorStateList(R.color.corLaranja_FFAA00));
            }

            if (item.getIDESLIKE() == 1) {
                holder.idLinearLike.setEnabled(false);
                holder.idLinearDeslike.setEnabled(false);
                holder.idTxwNaoCurti.setTextColor(Color.parseColor("#FFAA00"));
                holder.idAddDeslikePublication.setBackgroundTintList(context.getResources().getColorStateList(R.color.corLaranja_FFAA00));
            }

            if (!item.getIMAGE().equals("")){
                Picasso.with(context).load(item.getIMAGE()).into(holder.idImagePublication);
                holder.idImagePublication.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }else{
                holder.idLinearCard.removeView(holder.idImagePublication);
            }

            if (!item.getAVATAR().equals("")) {
                Picasso.with(context).load(item.getAVATAR()).into(holder.idPhotoProfile);
                holder.idPhotoProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            holder.idImagePublication.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("IMAGE",item.getIMAGE());
                    iniciarActivity(context,VwAcShowImageActivity.class,bundle);
                }
            });

            holder.idLinearLike.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    try {
                        item.setQTD_LIKES(item.getQTD_LIKES() + 1);;
                        holder.idLikesPublication.setText(item.getQTD_LIKES() + " Curtidas");
                        holder.idTxwCurti.setTextColor(Color.parseColor("#FFAA00"));
                        holder.idAddLikePulication.setBackgroundTintList(context.getResources().getColorStateList(R.color.corLaranja_FFAA00));
                        holder.idLinearLike.setEnabled(false);
                        holder.idLinearDeslike.setEnabled(false);

                        LIKE_SESSION.setID_PUBLICATION(item.getID_PUBLICATION());
                        LIKE_SESSION.setID_USER(USER_SESSION.getID_USER());

                        new WsSendLike(context).execute();
                    }catch (Exception ex){
                        excecoes(context,ex);
                    }
                }
            });

            holder.idLinearDeslike.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    try {
                        item.setQTD_DESLIKE(item.getQTD_DESLIKE() + 1);;
                        holder.idDeslikePulication.setText(item.getQTD_DESLIKE() + " não curtidas");
                        holder.idTxwNaoCurti.setTextColor(Color.parseColor("#FFAA00"));
                        holder.idAddDeslikePublication.setBackgroundTintList(context.getResources().getColorStateList(R.color.corLaranja_FFAA00));
                        holder.idLinearLike.setEnabled(false);
                        holder.idLinearDeslike.setEnabled(false);

                        DESLIKE_SESSION.setID_PUBLICATION(item.getID_PUBLICATION());
                        DESLIKE_SESSION.setID_USER(USER_SESSION.getID_USER());

                        new WsSendDeslike(context).execute();
                    }catch (Exception ex){
                        excecoes(context,ex);
                    }
                }
            });

            holder.idLinearComments.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putLong("ID_PUBLICATION",item.getID_PUBLICATION());
                    iniciarActivity(context, VwAcComments.class,bundle);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return obListPublication.getLISTA().size();
    }

    public void setRecyclerViewOnClickListenerHack(ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack r){
        recyclerViewOnClickListenerHack = r;
    }

    public void addListItem(ObPublication publication){
        obListPublication.getLISTA().add(publication);
        notifyItemInserted(obListPublication.getLISTA().size());
    }

    public void removeListItem(int position){
        obListPublication.getLISTA().remove(position);
        notifyItemRemoved(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public CircleImageView idPhotoProfile;
        public TextView ïdNameProfile;
        public TextView idLocationPublication;
        public TextView idMessagePublication;
        public ImageView idImagePublication;
        public TextView idLikesPublication;
        public TextView idDeslikePulication;
        public TextView idCommentsPulication;
        public CardView idCardView;
        public LinearLayout idLinearCard;
        public LinearLayout idLinearLike;
        public LinearLayout idLinearDeslike;
        public LinearLayout idLinearComments;
        public ImageButton idAddLikePulication;
        public TextView idTxwCurti;
        public ImageButton idAddDeslikePublication;
        public TextView idTxwNaoCurti;
        public TextView idTxwData;

        public MyViewHolder(View view) {
            super(view);
            idCardView = (CardView) view.findViewById(R.id.idCardViewPublication);
            idLinearCard = (LinearLayout) view.findViewById(R.id.idLlCard) ;
            idPhotoProfile =(CircleImageView) view.findViewById(R.id.idPhotoProfile);
            ïdNameProfile =(TextView) view.findViewById(R.id.idNameProfile);
            idLocationPublication =(TextView) view.findViewById(R.id.idLocationPublication);
            idMessagePublication =(TextView) view.findViewById(R.id.idMessagePublication);
            idImagePublication = (ImageView) view.findViewById(R.id.idImagePublication);
            idLikesPublication = (TextView) view.findViewById(R.id.idLikesPublication);
            idDeslikePulication = (TextView) view.findViewById(R.id.idDeslikePulication);
            idCommentsPulication = (TextView) view.findViewById(R.id.idCommentsPulication);
            idLinearLike = (LinearLayout) view.findViewById(R.id.idLyLike);
            idLinearDeslike = (LinearLayout) view.findViewById(R.id.idLyDeslike);
            idLinearComments = (LinearLayout) view.findViewById(R.id.idLyComents);
            idAddLikePulication = (ImageButton) view.findViewById(R.id.idAddLikePulication);
            idTxwCurti = (TextView) view.findViewById(R.id.idTxwCurti);
            idAddDeslikePublication = (ImageButton) view.findViewById(R.id.idAddDeslikePublication);
            idTxwNaoCurti = (TextView) view.findViewById(R.id.idTxwNaoCurti);
            idTxwData = (TextView) view.findViewById(R.id.idTxwData);
        }

        @Override
        public void onClick(View v) {
            if(recyclerViewOnClickListenerHack != null){
                recyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
