package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.business.BsMaps;
import br.com.unclephill.enjoy.repository.webservice.WsLocation;

import static br.com.unclephill.enjoy.app.ApFunctions.aplicarMascaras;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.MAPS;
import static br.com.unclephill.enjoy.app.ApSession.MASCARA_CEP;
import static br.com.unclephill.enjoy.app.ApSession.TB_LOCATION;

public class VwAcLocation extends Activity {

    private EditText idEdtCountry;
    private EditText idEdtState;
    private EditText idEdtCity;
    private EditText idEdtCEP;
    private Button idBtnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vw_ac_location);
        this.inflar();
        this.loadInformation();
        MAPS = new BsMaps(VwAcLocation.this);
    }

    private void inflar(){
        this.idEdtCountry = (EditText) findViewById(R.id.idEdtCountry);
        this.idEdtState = (EditText) findViewById(R.id.idEdtState);
        this.idEdtCity = (EditText) findViewById(R.id.idEdtCity);
        this.idEdtCEP = (EditText) findViewById(R.id.idEdtCEP);
        this.idBtnSave = (Button) findViewById(R.id.idBtnSave);
        aplicarMascaras(this.idEdtCEP,MASCARA_CEP);
    }

    private void loadInformation(){
        this.idEdtCountry.setText(LOCATION_SESSION.getCOUNTRY());
        this.idEdtState.setText(LOCATION_SESSION.getSTATE());
        this.idEdtCity.setText(LOCATION_SESSION.getCITY());
        this.idEdtCEP.setText(LOCATION_SESSION.getCEP());
    }

    public void onClickSalvar(View view){
        try{
            if (!this.validaView()){return;}
            LOCATION_SESSION.setCOUNTRY(this.idEdtCountry.getText().toString());
            LOCATION_SESSION.setSTATE(this.idEdtState.getText().toString());
            LOCATION_SESSION.setCITY(this.idEdtCity.getText().toString());
            LOCATION_SESSION.setCEP(this.idEdtCEP.getText().toString());
            LOCATION_SESSION.setSUCESS(true);
            LOCATION_SESSION.setMESSAGE("Sucesso!");
            if (LOCATION_SESSION.getLATITUDE() == 0
                    && LOCATION_SESSION.getLOGITUDE() == 0){
                new WsLocation(VwAcLocation.this, WsLocation.Type.ADDRESS).execute();
            }else{
                if (LOCATION_SESSION.getID_LOCATION() > 0){
                    TB_LOCATION.update(LOCATION_SESSION);
                }else{
                    LOCATION_SESSION.setID_LOCATION(TB_LOCATION.insert(LOCATION_SESSION));
                }
                modalNeutro(VwAcLocation.this,"Atenção!","Localização salva!","OK");
            }
        }catch (Exception ex){
            excecoes(VwAcLocation.this,ex);
        }
    }

    public void onClickCallBack(View view){
        fecharActivity(VwAcLocation.this);
    }

    public void onClickGetLocation(View view){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(VwAcLocation.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(VwAcLocation.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions(VwAcLocation.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                } else {
                    ActivityCompat.requestPermissions(VwAcLocation.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                }
            } else {
                if (!MAPS.checkGPS()) {
                    MAPS.showAlertActivateGPS();
                } else {
                    new WsLocation(VwAcLocation.this, WsLocation.Type.LOCATION).execute();
                }
            }
        }
    }

    private boolean validaView(){
        if (this.idEdtCountry.getText().equals("")){
            modalNeutro(VwAcLocation.this,"Atenção!","Informe o Pais.","OK");
            this.idEdtCountry.requestFocus();
            return false;
        }


        if (this.idEdtState.getText().toString().equals("")){
            modalNeutro(VwAcLocation.this,"Atenção!","Informe o estado.","OK");
            this.idEdtState.requestFocus();
            return false;
        }

        if (this.idEdtCity.getText().equals("")){
            modalNeutro(VwAcLocation.this,"Atenção!", "Informe a Cidade.","OK");
            this.idEdtCity.requestFocus();
            return false;
        }

        return true;
    }
}
