package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.jar.Manifest;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.object.ObPublication;
import br.com.unclephill.enjoy.repository.webservice.WsNewPublication;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.unclephill.enjoy.app.ApFunctions.aplicarMascaras;
import static br.com.unclephill.enjoy.app.ApFunctions.base64ParseImage;
import static br.com.unclephill.enjoy.app.ApFunctions.excecoes;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.formatDate;
import static br.com.unclephill.enjoy.app.ApFunctions.imageParseBase64;
import static br.com.unclephill.enjoy.app.ApFunctions.iniciarActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApSession.IMAGEM_CAMERA;
import static br.com.unclephill.enjoy.app.ApSession.IMAGEM_INTERNA;
import static br.com.unclephill.enjoy.app.ApSession.LOCATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.MASCARA_CELULAR;
import static br.com.unclephill.enjoy.app.ApSession.MASCARA_DATA;
import static br.com.unclephill.enjoy.app.ApSession.PUBLICATION_SESSION;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class VwAcNewPublication extends Activity {
    private int ID_DIALOG = 0;
    private ImageView idImgPublication;
    private TextView idLocationPublication;
    private EditText idEdtPublication;
    private Button idBtnPublication;
    private TextView idNameProfile;
    private CircleImageView idPhotoProfile;
    private ObPublication obPublication;
    private Boolean bTakePhoto;
    private LinearLayout idllInsertPhoto;
    private ImageButton idBtnClosePhoto;
    private TextView idTxwData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.inflar();
    }

    @Override
    protected void onResume(){
        super.onResume();
        this.loadInformation();
    }

    public void inflar(){
        setContentView(R.layout.vw_ac_new_publication);
        this.obPublication = new ObPublication();
        this.idNameProfile = (TextView) findViewById(R.id.idNameProfile);
        this.idImgPublication = (ImageView) findViewById(R.id.idImgPublication);
        this.idLocationPublication = (TextView) findViewById(R.id.idLocationPublication);
        this.idEdtPublication = (EditText) findViewById(R.id.idEdtPublication);
        this.idBtnPublication = (Button) findViewById(R.id.idBtnPublication);
        this.idPhotoProfile = (CircleImageView) findViewById(R.id.idPhotoProfile);
        this.idllInsertPhoto = (LinearLayout) findViewById(R.id.idllInsertPhoto);
        this.idBtnClosePhoto = (ImageButton) findViewById(R.id.idBtnClosePhoto);
        this.idTxwData = (TextView) findViewById(R.id.idTxwData);
        this.idBtnClosePhoto.setVisibility(View.INVISIBLE);
        this.bTakePhoto = false;
    }

    public void loadInformation(){
        this.idTxwData.setText(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "/"
        + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "/"
        + Calendar.getInstance().get(Calendar.YEAR));
        Bitmap bmp = base64ParseImage(USER_SESSION.getAVATAR().replace("data:image/jpg;base64,",""));
        if (bmp!=null){
            this.idPhotoProfile.setScaleType(CircleImageView.ScaleType.CENTER_CROP);
            this.idPhotoProfile.setImageBitmap(bmp);
        }else{
            if(!USER_SESSION.getAVATAR().equals("")){
                Picasso.with(this).load(USER_SESSION.getAVATAR()).into(this.idPhotoProfile);
                this.idPhotoProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }

        if (USER_SESSION.isSUCESS()){
            this.idNameProfile.setText(USER_SESSION.getNAME() + " " + USER_SESSION.getLAST_NAME());
        }else{
            this.idNameProfile.setText("Seu Nome");
        }

        if (LOCATION_SESSION.isSUCESS()) {
            this.idLocationPublication.setText(LOCATION_SESSION.getCITY() + " - " + LOCATION_SESSION.getSTATE());
        }else{
            this.idLocationPublication.setText("Localização");
        }
    }

    public void onClickCallBack(View view){
        fecharActivity(VwAcNewPublication.this);
    }

    public void onClickTakePhoto(View view){
        AlertDialog alert;
        ArrayList<String> itens = new ArrayList<String>();

        itens.add("Tirar foto");
        itens.add("Escolher da galeria");

        ArrayAdapter adapter = new ArrayAdapter(VwAcNewPublication.this, R.layout.vw_it_item_list, itens);
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(VwAcNewPublication.this);
        builder.setSingleChoiceItems(adapter, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idOpcao) {
                Intent intent;
                switch (idOpcao){
                    case 0: //Tirar foto:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(VwAcNewPublication.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(VwAcNewPublication.this, android.Manifest.permission.CAMERA)) {
                                    ActivityCompat.requestPermissions(VwAcNewPublication.this, new String[]{android.Manifest.permission.CAMERA}, 0);
                                } else {
                                    ActivityCompat.requestPermissions(VwAcNewPublication.this, new String[]{android.Manifest.permission.CAMERA}, 0);
                                }
                            } else {
                                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, IMAGEM_CAMERA);
                                dialog.cancel();
                            }
                        }
                        break;
                    case 1://Pegar da galeria
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(VwAcNewPublication.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(VwAcNewPublication.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                    ActivityCompat.requestPermissions(VwAcNewPublication.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                } else {
                                    ActivityCompat.requestPermissions(VwAcNewPublication.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                }
                            } else {
                                intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType("image/*");
                                startActivityForResult(intent,IMAGEM_INTERNA);
                                dialog.cancel();
                            }
                        }
                        break;
                }
            }
        });
        alert = builder.create();
        alert.show();
    }

    public void onClickGetLocation(View view){
        iniciarActivity(VwAcNewPublication.this,VwAcLocation.class,null);
    }

    public void onClickPublication(View view){
        try{
            if (!this.validaView()){return;}
            PUBLICATION_SESSION = new ObPublication();
            PUBLICATION_SESSION.setID_USER(USER_SESSION.getID_USER());
            PUBLICATION_SESSION.setDESCRIPTION(this.idEdtPublication.getText().toString());
            PUBLICATION_SESSION.setCOUNTRY(LOCATION_SESSION.getCOUNTRY());
            PUBLICATION_SESSION.setSTATE(LOCATION_SESSION.getSTATE());
            PUBLICATION_SESSION.setCITY(LOCATION_SESSION.getCITY());
            PUBLICATION_SESSION.setDISTRICT(LOCATION_SESSION.getDISTRICT());
            PUBLICATION_SESSION.setCEP(LOCATION_SESSION.getCEP());
            PUBLICATION_SESSION.setLATITUDE(LOCATION_SESSION.getLATITUDE());
            PUBLICATION_SESSION.setLONGITUDE(LOCATION_SESSION.getLOGITUDE());
            if (this.bTakePhoto){
                PUBLICATION_SESSION.setIMAGE(imageParseBase64(((BitmapDrawable) idImgPublication.getDrawable()).getBitmap()));
            }

            new WsNewPublication(VwAcNewPublication.this){
                @Override
                protected void onPostExecute(Void voids) {
                    super.onPostExecute(voids);
                    if (PUBLICATION_SESSION.getSUCESS()){
                        Toast.makeText(VwAcNewPublication.this,"Publicação enviada!",Toast.LENGTH_SHORT);
                        fecharActivity(VwAcNewPublication.this);
                    }else{
                        modalNeutro(VwAcNewPublication.this,"Atenção!",PUBLICATION_SESSION.getMESSAGE(),"OK");
                    }
                    fecharProgresso();
                }
            }.execute();
        }catch (Exception ex){
            modalNeutro(VwAcNewPublication.this,"Atenção!",ex.getMessage(),"OK");
        }
    }

    public void onClickClosePhoto(View view){
        this.idImgPublication.setImageBitmap(null);
        this.idBtnClosePhoto.setVisibility(View.INVISIBLE);
        this.idllInsertPhoto.setVisibility(View.VISIBLE);
        this.bTakePhoto = false;
    }

    public void onClickDataEvent(View view){
            showDialog(ID_DIALOG);
    }

    @Override
    protected Dialog onCreateDialog(int id){
        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);

        if (id == ID_DIALOG){
            return new DatePickerDialog(this,mDateSetListener,ano,mes,dia);
        }

        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String data = String.valueOf(dayOfMonth) + "/"
                    + String.valueOf(monthOfYear + 1) + "/"
                    + String.valueOf(year);
            idTxwData.setText(data);
        }
    };

    public Boolean validaView(){
        if (this.idEdtPublication.getText().toString().equals("")){
            modalNeutro(this,"Atenção!","Descreva seu evento.","OK");
            this.idEdtPublication.requestFocus();
            return false;
        }

        if (LOCATION_SESSION.getID_LOCATION() <= 0 ){
            modalNeutro(this,"Atenção!","Informe uma localização para seu evento!","OK");
            return false;
        }

        return  true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if (resultCode == RESULT_OK){
                this.bTakePhoto = true;
                Bitmap bpm = null;
                this.idllInsertPhoto.setVisibility(View.INVISIBLE);
                this.idBtnClosePhoto.setVisibility(View.VISIBLE);
                if (requestCode == IMAGEM_INTERNA){
                    Uri imagemSelecionada = data.getData();
                    String[] colunas = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(imagemSelecionada, colunas, null, null, null);
                    cursor.moveToFirst();

                    int indexColuna = cursor.getColumnIndex(colunas[0]);
                    String pathImg = cursor.getString(indexColuna);
                    cursor.close();

                    bpm = BitmapFactory.decodeFile(pathImg);
                    this.idImgPublication.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    this.idImgPublication.setImageBitmap(bpm);

                }else if(requestCode == IMAGEM_CAMERA){
                    bpm = (Bitmap) data.getExtras().get("data");
                    this.idImgPublication.setImageBitmap(bpm);
                    this.idImgPublication.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            }
        }catch (Exception ex){excecoes(this,ex);}
    }
}
