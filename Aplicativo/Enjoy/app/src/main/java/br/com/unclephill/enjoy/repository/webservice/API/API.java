package br.com.unclephill.enjoy.repository.webservice.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    private static final Retrofit RETROFIT = new Retrofit
            .Builder()
            .baseUrl("http://enjoys.96.lt/rest/index.php/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
