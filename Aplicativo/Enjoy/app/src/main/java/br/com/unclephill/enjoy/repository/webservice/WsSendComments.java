package br.com.unclephill.enjoy.repository.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsComments;
import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;
import static br.com.unclephill.enjoy.app.ApSession.COMMENTS_SESSION;

public class WsSendComments extends AsyncTask<Void,Void,Void> {
    private Context context;
    private BsComments bsComments;
    private String URL = URL_BASE + "feed/comment/";
    private Boolean Status;

    public WsSendComments(Context context){
        this.context = context;
        this.Status = false;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        mostrarProgresso(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (!Status){
            Status = true;
            try{
                this.bsComments = new BsComments(COMMENTS_SESSION);
                this.bsComments.setJSON(
                        Connection(URL,this.bsComments.getJSON(),
                                ApConnection.TypeConnection.POST));

            }catch (Exception ex){
                COMMENTS_SESSION.setSUCESS(false);
                COMMENTS_SESSION.setMESSAGE(ex.getMessage());
            }finally {
                Status = false;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
        if (COMMENTS_SESSION.isSUCESS()){
            Toast.makeText(context,"Comentário enviado!",Toast.LENGTH_SHORT).show();
            fecharActivity(context);
        }else{
            modalNeutro(context,"Atenção",COMMENTS_SESSION.getMESSAGE(),"OK");
        }
        fecharProgresso();
    }
}
