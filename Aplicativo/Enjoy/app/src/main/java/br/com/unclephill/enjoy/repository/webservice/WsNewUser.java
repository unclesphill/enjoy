package br.com.unclephill.enjoy.repository.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import br.com.unclephill.enjoy.app.ApConnection;
import br.com.unclephill.enjoy.business.BsUser;
import static br.com.unclephill.enjoy.app.ApConnection.Connection;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharProgresso;
import static br.com.unclephill.enjoy.app.ApFunctions.modalNeutro;
import static br.com.unclephill.enjoy.app.ApFunctions.mostrarProgresso;
import static br.com.unclephill.enjoy.app.ApSession.URL_BASE;
import static br.com.unclephill.enjoy.app.ApSession.USER_SESSION;

public class WsNewUser extends AsyncTask<Void,Void,Void>  {
    private Context context;
    private BsUser bsUser;
    private String URL = URL_BASE + "users/newuser/";

    public WsNewUser(Context context){ this.context = context;}

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        mostrarProgresso(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try{
            this.bsUser = new BsUser(USER_SESSION);
            this.bsUser.setJSON(
                    Connection(URL,this.bsUser.getJSON(),
                            ApConnection.TypeConnection.POST));
        }catch (Exception ex){
            USER_SESSION.setSUCESS(false);
            USER_SESSION.setMESSAGE(ex.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        super.onPostExecute(voids);
        if (USER_SESSION.isSUCESS()){
            Toast.makeText(context,"Sucesso!",Toast.LENGTH_LONG).show();
            fecharActivity(context);
        }else{
            modalNeutro(context,"Atenção", USER_SESSION.getMESSAGE(),"OK");
        }
        fecharProgresso();
    }
}
