package br.com.unclephill.enjoy.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import br.com.unclephill.enjoy.app.ApDatabase;
import br.com.unclephill.enjoy.object.ObUser;

public class TbUser {

    private SQLiteDatabase DB_ENJOY;
    private String TB_USER = "USER";

    private enum FIELDS{
        ID_USER,
        NAME,
        LAST_NAME,
        EMAIL,
        PHONE,
        PASSWORD,
        AVATAR
    }

    public TbUser(Context context){
        ApDatabase database = new ApDatabase(context);
        this.DB_ENJOY = database.getWritableDatabase();
    }

    public long insert(ObUser obUser){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(FIELDS.ID_USER.name(),obUser.getID_USER());
            contentValues.put(FIELDS.NAME.name(),obUser.getNAME());
            contentValues.put(FIELDS.LAST_NAME.name(),obUser.getLAST_NAME());
            contentValues.put(FIELDS.EMAIL.name(),obUser.getEMAIL());
            contentValues.put(FIELDS.PHONE.name(),obUser.getPHONE());
            contentValues.put(FIELDS.PASSWORD.name(),obUser.getPASSWORD());
            contentValues.put(FIELDS.AVATAR.name(),obUser.getAVATAR());

            return this.DB_ENJOY.insert(this.TB_USER,null,contentValues);
        }catch (Exception e){
            return 0;
        }
    }

    public Boolean update(ObUser obUser){
        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(FIELDS.NAME.name(), obUser.getNAME());
            contentValues.put(FIELDS.LAST_NAME.name(), obUser.getLAST_NAME());
            contentValues.put(FIELDS.EMAIL.name(), obUser.getEMAIL());
            contentValues.put(FIELDS.PHONE.name(), obUser.getPHONE());
            contentValues.put(FIELDS.PASSWORD.name(), obUser.getPASSWORD());
            contentValues.put(FIELDS.AVATAR.name(), obUser.getAVATAR());

            this.DB_ENJOY.update(this.TB_USER, contentValues, FIELDS.ID_USER.name() + " = ? ",
                    new String[]{Long.toString(obUser.getID_USER())});

            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public Boolean delete(ObUser obUser) {
        try {
            if (obUser.getID_USER() > 0) {
                this.DB_ENJOY.delete(this.TB_USER, FIELDS.ID_USER.name() + " = ? ",
                        new String[]{Long.toString(obUser.getID_USER())});
            }
            return  true;
        }catch (Exception ex){
            return false;
        }
    }

    public Boolean deleteAll(){
        try {
            this.DB_ENJOY.delete(this.TB_USER, null,null);
            return  true;
        }catch (Exception ex){
            return false;
        }
    }

    public List<ObUser> select(){
        try{
            List<ObUser> list = new ArrayList<ObUser>();
            String[] columns = new String[]{
                    FIELDS.ID_USER.name(),
                    FIELDS.NAME.name(),
                    FIELDS.LAST_NAME.name(),
                    FIELDS.EMAIL.name(),
                    FIELDS.PHONE.name(),
                    FIELDS.PASSWORD.name(),
                    FIELDS.AVATAR.name()};

            Cursor cursor = this.DB_ENJOY.query(this.TB_USER,columns,null,null,null,null,FIELDS.NAME.name() + " ASC");

            if (cursor.getCount()>0){
                cursor.moveToFirst();
                ObUser obUser;
                do{
                    obUser = new ObUser();

                    obUser.setID_USER(cursor.getInt(0));
                    obUser.setNAME(cursor.getString(1));
                    obUser.setLAST_NAME(cursor.getString(2));
                    obUser.setEMAIL(cursor.getString(3));
                    obUser.setPHONE(cursor.getString(4));
                    obUser.setPASSWORD(cursor.getString(5));
                    obUser.setAVATAR(cursor.getString(6));
                    obUser.setMESSAGE("Sucesso!");
                    obUser.setSUCESS(true);

                    list.add(obUser);
                }while (cursor.moveToNext());
            }
            return list;
        }catch (Exception ex){
            return null;
        }
    }
}

