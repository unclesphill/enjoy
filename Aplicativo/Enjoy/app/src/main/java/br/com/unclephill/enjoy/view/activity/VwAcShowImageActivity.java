package br.com.unclephill.enjoy.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import br.com.unclephill.enjoy.R;

import static br.com.unclephill.enjoy.app.ApFunctions.byteParseBitmap;
import static br.com.unclephill.enjoy.app.ApFunctions.fecharActivity;

public class VwAcShowImageActivity extends Activity {

    private ImageView imgView;
    private Bitmap bmpView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.vw_ac_show_image);
        this.inflar();
        this.loadInformation();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable("IMAGE",this.bmpView);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.bmpView = savedInstanceState.getParcelable("IMAGE");
    }

    public void inflar(){
        this.imgView = (ImageView) findViewById(R.id.idImage);
    }

    public void loadInformation(){
        Intent intent = getIntent();
        if (!intent.getExtras().getString("IMAGE").equals("")){
            Picasso.with(VwAcShowImageActivity.this).load(intent.getExtras().getString("IMAGE")).into(this.imgView);
            this.imgView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }else{
            Toast.makeText(VwAcShowImageActivity.this,"Não há imagem para mostrar!",Toast.LENGTH_LONG).show();
            fecharActivity(VwAcShowImageActivity.this);
        }
    }
}
