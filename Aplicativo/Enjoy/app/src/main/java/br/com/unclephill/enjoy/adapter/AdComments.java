package br.com.unclephill.enjoy.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.unclephill.enjoy.R;
import br.com.unclephill.enjoy.app.ApFunctions;
import br.com.unclephill.enjoy.object.ObComments;
import br.com.unclephill.enjoy.object.ObListComments;
import br.com.unclephill.enjoy.object.ObPublication;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.unclephill.enjoy.app.ApFunctions.base64ParseImage;
import static br.com.unclephill.enjoy.app.ApFunctions.formatDate;

public class AdComments extends RecyclerView.Adapter<AdComments.MyViewHolder>{
    private Context context;
    private ObListComments obListComments;
    private LayoutInflater layoutInflater;
    private float scale;
    private int width;
    private int height;
    private ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack recyclerViewOnClickListenerHack;

    public AdComments(Context context, ObListComments obListComments){
        this.context = context;
        this.obListComments = obListComments;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.scale = this.context.getResources().getDisplayMetrics().density;
        this.width = this.context.getResources().getDisplayMetrics().widthPixels - (int)(14 * this.scale + 0.5f);
        this.height = (this.width / 16) * 9;
    }

    public void setRecyclerViewOnClickListenerHack(ApFunctions.RecyclerViewTouchListener.RecyclerViewOnClickListenerHack r){
        recyclerViewOnClickListenerHack = r;
    }

    public void addListItem(ObComments comments){
        obListComments.setLISTA(comments);
        notifyItemInserted(obListComments.getLISTA().size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = this.layoutInflater.inflate(R.layout.vw_lt_item_comments, parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ObComments item = obListComments.getLISTA(holder.getLayoutPosition());
        if (holder.itemView.getTag() == null){
            holder.itemView.setTag(item);
            holder.idNameProfile.setText(item.getNAME());
            holder.idMessage.setText(item.getDESCRIPTION());
            holder.idDateHours.setText(item.getDATE());
            Bitmap bmp = base64ParseImage(item.getAVATAR());
            if (bmp != null){
                holder.idPhotoProfile.setImageBitmap(bmp);
            }
            if (!item.getAVATAR().equals("")){
                Picasso.with(context).load(item.getAVATAR()).into(holder.idPhotoProfile);
                holder.idPhotoProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
    }

    @Override
    public int getItemCount() {
        return obListComments.getLISTA().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public CircleImageView idPhotoProfile;
        public TextView idNameProfile;
        public TextView idMessage;
        public TextView idDateHours;

        public MyViewHolder(View view){
            super(view);
            idPhotoProfile = (CircleImageView) view.findViewById(R.id.idPhotoProfile);
            idNameProfile = (TextView) view.findViewById(R.id.idNameProfile);
            idMessage = (TextView) view.findViewById(R.id.idMessage);
            idDateHours = (TextView) view.findViewById(R.id.idDateHours);
        }

        @Override
        public void onClick(View view) {
            if(recyclerViewOnClickListenerHack != null){
                recyclerViewOnClickListenerHack.onClickListener(view, getPosition());
            }
        }
    }
}
