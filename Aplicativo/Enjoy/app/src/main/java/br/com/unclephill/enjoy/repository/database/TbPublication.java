package br.com.unclephill.enjoy.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.com.unclephill.enjoy.app.ApDatabase;
import br.com.unclephill.enjoy.object.ObListPublication;
import br.com.unclephill.enjoy.object.ObPublication;

public class TbPublication {
    private SQLiteDatabase DB_ENJOY;
    public String TB_PUBLICATION = "PUBLICATION";

    public enum FIELDS{
        NAME,
        DISTANCIA,
        ID_PUBLICATION,
        ID_USER,
        DESCRIPTION,
        DATE,
        IMAGE,
        COUNTRY,
        CITY,
        DISTRICT,
        STATE,
        CEP,
        LATITUDE,
        LONGITUDE,
        QTD_COMMENTS,
        QTD_DESLIKE,
        QTD_LIKES,
        AVATAR,
        LAST_NAME,
        ILIKE,
        IDESLIKE;
    }

    public TbPublication(Context context){
        ApDatabase database = new ApDatabase(context);
        this.DB_ENJOY = database.getWritableDatabase();
    }

    public long insert(ObPublication obPublication){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(TbPublication.FIELDS.ID_PUBLICATION.name(),obPublication.getID_PUBLICATION());
            contentValues.put(TbPublication.FIELDS.ID_USER.name(),obPublication.getID_USER());
            contentValues.put(TbPublication.FIELDS.DESCRIPTION.name(),obPublication.getDESCRIPTION());
            contentValues.put(TbPublication.FIELDS.DATE.name(),obPublication.getDATE());
            contentValues.put(TbPublication.FIELDS.IMAGE.name(),obPublication.getIMAGE());
            contentValues.put(TbPublication.FIELDS.COUNTRY.name(),obPublication.getCOUNTRY());
            contentValues.put(TbPublication.FIELDS.CITY.name(),obPublication.getCITY());
            contentValues.put(TbPublication.FIELDS.DISTRICT.name(),obPublication.getDISTRICT());
            contentValues.put(TbPublication.FIELDS.STATE.name(),obPublication.getSTATE());
            contentValues.put(TbPublication.FIELDS.CEP.name(),obPublication.getCEP());
            contentValues.put(TbPublication.FIELDS.LATITUDE.name(),obPublication.getLATITUDE());
            contentValues.put(TbPublication.FIELDS.LONGITUDE.name(),obPublication.getLONGITUDE());
            contentValues.put(TbPublication.FIELDS.NAME.name(),obPublication.getNAME());
            contentValues.put(TbPublication.FIELDS.DISTANCIA.name(),obPublication.getDISTANCIA());
            contentValues.put(TbPublication.FIELDS.QTD_COMMENTS.name(),obPublication.getQTD_COMMENTS());
            contentValues.put(TbPublication.FIELDS.QTD_DESLIKE.name(),obPublication.getQTD_DESLIKE());
            contentValues.put(TbPublication.FIELDS.QTD_LIKES.name(),obPublication.getQTD_LIKES());
            contentValues.put(TbPublication.FIELDS.AVATAR.name(),obPublication.getAVATAR());
            contentValues.put(TbPublication.FIELDS.LAST_NAME.name(),obPublication.getLAST_NAME());
            contentValues.put(TbPublication.FIELDS.ILIKE.name(),obPublication.getILIKE());
            contentValues.put(TbPublication.FIELDS.IDESLIKE.name(),obPublication.getIDESLIKE());

            return this.DB_ENJOY.insert(this.TB_PUBLICATION,null,contentValues);
        }catch (Exception e){
            return 0;
        }
    }

    public Boolean update(ObPublication obPublication){
        try{
            ContentValues contentValues = new ContentValues();

            contentValues.put(TbPublication.FIELDS.ID_USER.name(),obPublication.getID_USER());
            contentValues.put(TbPublication.FIELDS.DESCRIPTION.name(),obPublication.getDESCRIPTION());
            contentValues.put(TbPublication.FIELDS.DATE.name(),obPublication.getDATE().toString());
            contentValues.put(TbPublication.FIELDS.IMAGE.name(),obPublication.getIMAGE());
            contentValues.put(TbPublication.FIELDS.COUNTRY.name(),obPublication.getCOUNTRY());
            contentValues.put(TbPublication.FIELDS.CITY.name(),obPublication.getCITY());
            contentValues.put(TbPublication.FIELDS.DISTRICT.name(),obPublication.getDISTRICT());
            contentValues.put(TbPublication.FIELDS.STATE.name(),obPublication.getSTATE());
            contentValues.put(TbPublication.FIELDS.CEP.name(),obPublication.getCEP());
            contentValues.put(TbPublication.FIELDS.LATITUDE.name(),obPublication.getLATITUDE());
            contentValues.put(TbPublication.FIELDS.LONGITUDE.name(),obPublication.getLONGITUDE());
            contentValues.put(TbPublication.FIELDS.NAME.name(),obPublication.getNAME());
            contentValues.put(TbPublication.FIELDS.DISTANCIA.name(),obPublication.getDISTANCIA());
            contentValues.put(TbPublication.FIELDS.QTD_COMMENTS.name(),obPublication.getQTD_COMMENTS());
            contentValues.put(TbPublication.FIELDS.QTD_DESLIKE.name(),obPublication.getQTD_DESLIKE());
            contentValues.put(TbPublication.FIELDS.QTD_LIKES.name(),obPublication.getQTD_LIKES());
            contentValues.put(TbPublication.FIELDS.AVATAR.name(),obPublication.getAVATAR());
            contentValues.put(TbPublication.FIELDS.LAST_NAME.name(),obPublication.getLAST_NAME());
            contentValues.put(TbPublication.FIELDS.ILIKE.name(),obPublication.getILIKE());
            contentValues.put(TbPublication.FIELDS.IDESLIKE.name(),obPublication.getIDESLIKE());

            this.DB_ENJOY.update(this.TB_PUBLICATION,contentValues, TbPublication.FIELDS.ID_PUBLICATION.name() + " = ? ",
                    new String[]{Long.toString(obPublication.getID_PUBLICATION())});
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public Boolean delete(ObPublication obPublication){
        try{
            this.DB_ENJOY.delete(this.TB_PUBLICATION, TbPublication.FIELDS.ID_PUBLICATION.name() + " = ? ",
                    new String[]{Long.toString(obPublication.getID_PUBLICATION())});
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public Boolean deleteAll(){
        this.DB_ENJOY.delete(this.TB_PUBLICATION, null,null);
        return true;
    }

    public ObListPublication select(String condicao){
        try{
            String[] columns = new String[]{
                    TbPublication.FIELDS.ID_PUBLICATION.name(),
                    TbPublication.FIELDS.ID_USER.name(),
                    TbPublication.FIELDS.DESCRIPTION.name(),
                    TbPublication.FIELDS.DATE.name(),
                    TbPublication.FIELDS.IMAGE.name(),
                    TbPublication.FIELDS.COUNTRY.name(),
                    TbPublication.FIELDS.CITY.name(),
                    TbPublication.FIELDS.DISTRICT.name(),
                    TbPublication.FIELDS.STATE.name(),
                    TbPublication.FIELDS.CEP.name(),
                    TbPublication.FIELDS.LATITUDE.name(),
                    TbPublication.FIELDS.LONGITUDE.name(),
                    TbPublication.FIELDS.NAME.name(),
                    TbPublication.FIELDS.DISTANCIA.name(),
                    TbPublication.FIELDS.QTD_COMMENTS.name(),
                    TbPublication.FIELDS.QTD_DESLIKE.name(),
                    TbPublication.FIELDS.QTD_LIKES.name(),
                    TbPublication.FIELDS.AVATAR.name(),
                    TbPublication.FIELDS.LAST_NAME.name(),
                    TbPublication.FIELDS.ILIKE.name(),
                    TbPublication.FIELDS.IDESLIKE.name()
            };

            Cursor cursor;
            if (!condicao.equals("")){
                cursor = this.DB_ENJOY.query(this.TB_PUBLICATION,columns,
                        condicao ,null,null,null,null);
            }else{
                cursor = this.DB_ENJOY.query(this.TB_PUBLICATION,columns,
                        null ,null,null,null,null);
            }

            return readInformation(cursor);
        }catch (Exception ex){
            return null;
        }
    }

    public ObListPublication query(String query){
        return readInformation(this.DB_ENJOY.rawQuery(query,null));
    }

    private ObListPublication readInformation(Cursor cursor){
        ObListPublication list = new ObListPublication();
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            ObPublication obPublication;
            do{
                obPublication = new ObPublication();
                obPublication.setID_PUBLICATION(cursor.getLong(0));
                obPublication.setID_USER(cursor.getLong(1));
                obPublication.setDESCRIPTION(cursor.getString(2));
                obPublication.setDATE(cursor.getString(3));
                obPublication.setIMAGE(cursor.getString(4));
                obPublication.setCOUNTRY(cursor.getString(5));
                obPublication.setSTATE(cursor.getString(6));
                obPublication.setCITY(cursor.getString(7));
                obPublication.setDISTRICT(cursor.getString(8));
                obPublication.setCEP(cursor.getString(9));
                obPublication.setLATITUDE(cursor.getDouble(10));
                obPublication.setLONGITUDE(cursor.getDouble(11));
                obPublication.setNAME(cursor.getString(12));
                obPublication.setDISTANCIA(cursor.getInt(13));
                obPublication.setQTD_COMMENTS(cursor.getInt(14));
                obPublication.setQTD_DESLIKE(cursor.getInt(15));
                obPublication.setQTD_LIKES(cursor.getInt(16));
                obPublication.setAVATAR(cursor.getString(17));
                obPublication.setLAST_NAME(cursor.getString(18));
                obPublication.setILIKE(cursor.getInt(19));
                obPublication.setIDESLIKE(cursor.getInt(20));

                list.setLISTA(obPublication);
                list.setINDEX(10);
                list.setSUCCESS(true);
                list.setMESSAGE("Sucesso!");
            }while (cursor.moveToNext());
        }
        return list;
    }
}
