package br.com.unclephill.enjoy.object;

import java.util.Date;

import static br.com.unclephill.enjoy.app.ApFunctions.Quotes;

public class ObUser {
    private long ID_USER;
    private String NAME;
    private String LAST_NAME;
    private String EMAIL;
    private String PHONE;
    private String PASSWORD;
    private String AVATAR;
    private boolean SUCESS;
    private String MESSAGE;

    public ObUser(long ID_USER,String NAME,String LAST_NAME,String EMAIL,
                  String PHONE, String PASSWORD, Date REGISTER_DATE,
                  String AVATAR,String MESSAGE,Boolean SUCESS){
        this.ID_USER = ID_USER;
        this.NAME = NAME;
        this.LAST_NAME = LAST_NAME;
        this.EMAIL = EMAIL;
        this.PHONE = PHONE;
        this.PASSWORD = PASSWORD;
        this.AVATAR = AVATAR;
        this.MESSAGE = MESSAGE;
        this.SUCESS = SUCESS;
    }

    public ObUser(){
        this.ID_USER = 0;
        this.NAME = "";
        this.LAST_NAME = "";
        this.EMAIL = "";
        this.PHONE = "";
        this.PASSWORD = "";
        this.AVATAR = "";
        this.MESSAGE = "";
        this.SUCESS = false;
    }

    public long getID_USER() {
        return ID_USER;
    }

    public void setID_USER(long ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getNAME() {
        return Quotes(NAME);
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getLAST_NAME() {
        return Quotes(LAST_NAME);
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getEMAIL() {
        return Quotes(EMAIL);
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPHONE() {
        return Quotes(PHONE);
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getPASSWORD() {
        return Quotes(PASSWORD);
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getAVATAR() {
        return AVATAR;
    }

    public void setAVATAR(String AVATAR) {
        this.AVATAR = AVATAR;
    }

    public boolean isSUCESS() {
        return SUCESS;
    }

    public void setSUCESS(boolean STATUS) {
        this.SUCESS = STATUS;
    }

    public String getMESSAGE() {
        return Quotes(MESSAGE);
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }
}
